class ObjectList {
  String? message;
  int? status;
  List<Data>? data;

  ObjectList({this.message, this.status, this.data});

  ObjectList.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  int? objectId;
  String? objectLabel;
  String? objectName;
  String? objectAddress;

  Data({this.objectId, this.objectLabel, this.objectName, this.objectAddress});

  Data.fromJson(Map<String, dynamic> json) {
    objectId = json['object_id'];
    objectLabel = json['object_label'];
    objectName = json['object_name'];
    objectAddress = json['object_address'];
  }
}

class ObjectListRequest {
  ObjectListRequest({
    this.phone_number,
    this.organization_id,
  });
  int? organization_id;
  String? phone_number;
}
