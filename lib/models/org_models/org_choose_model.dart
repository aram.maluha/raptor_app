// To parse this JSON data, do
//
//     final authFirstStep = authFirstStepFromJson(jsonString);

import 'dart:convert';

OrganizationsChoose organizationsChooseFromJson(String str) =>
    OrganizationsChoose.fromJson(json.decode(str));

class OrganizationsChoose {
  OrganizationsChoose({
    this.message,
    this.status,
  });

  String? message;
  int? status;

  factory OrganizationsChoose.fromJson(Map<String, dynamic> json) =>
      OrganizationsChoose(
        message: json["message"],
        status: json["status"],
      );
}

class OrgChoose {
  OrgChoose({
    this.organization_id,
    this.phone_number,
  });
  int? organization_id;
  String? phone_number;
}
