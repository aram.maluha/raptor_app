class OrganizationsListAnswer {
  String? message;
  int? status;
  List<Data>? data;

  OrganizationsListAnswer({this.message, this.status, this.data});

  OrganizationsListAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  int? organizationId;
  int? organizationNumber;
  String? organizationName;

  Data({
    this.organizationId,
    this.organizationNumber,
    this.organizationName,
  });

  Data.fromJson(Map<String, dynamic> json) {
    organizationId = json['organization_id'];
    organizationNumber = json['organization_number'];
    organizationName = json['organization_name'];
  }
}

class OrgListRequest {
  OrgListRequest({this.phone_number});
  String? phone_number;
}
