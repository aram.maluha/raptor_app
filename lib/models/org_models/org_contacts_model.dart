class OrgContactsAnswer {
  String? message;
  int? status;
  Data? data;

  OrgContactsAnswer({this.message, this.status, this.data});

  OrgContactsAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }
}

class Data {
  int? id;
  String? name;
  List<Contacts>? contacts;
  String? image;

  Data({this.id, this.name, this.contacts, this.image});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    if (json['contacts'] != null) {
      contacts = <Contacts>[];
      json['contacts'].forEach((v) {
        contacts?.add(new Contacts.fromJson(v));
      });
    }
    image = json['image'];
  }
}

class Contacts {
  String? type;
  String? value;

  Contacts({this.type, this.value});

  Contacts.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    value = json['value'];
  }
}

class OrgContactsRequest {
  OrgContactsRequest({
    this.organization_id,
    this.phone_number,
  });
  int? organization_id;
  String? phone_number;
}
