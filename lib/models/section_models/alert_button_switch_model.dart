class AlertButtonSwitch {
  String? message;
  int? status;
  Data? data;

  AlertButtonSwitch({this.message, this.status, this.data});

  AlertButtonSwitch.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }
}

class Data {
  String? timeout;
  String? timeoutKey;
  bool? result;

  Data({this.timeout, this.timeoutKey, this.result});

  Data.fromJson(Map<String, dynamic> json) {
    timeout = json['timeout'];
    timeoutKey = json['timeout_key'];
    result = json['result'];
  }
}

// class AlertButtonSwitch {
//   AlertButtonSwitch({
//     this.message,
//     this.status,
//   });

//   String? message;
//   int? status;

//   factory AlertButtonSwitch.fromJson(Map<String, dynamic> json) =>
//       AlertButtonSwitch(
//         message: json["message"],
//         status: json["status"],
//       );
// }

class AlertButtonSwitchRequest {
  AlertButtonSwitchRequest({
    this.button_id,
    this.timeout_key,
  });
  int? button_id;
  String? timeout_key;
}
