class NotificationListAnswer {
  String? message;
  int? status;
  List<Data>? data;

  NotificationListAnswer({this.message, this.status, this.data});

  NotificationListAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  String? threats;
  String? security;
  String? tech;

  Data({this.threats, this.security, this.tech});

  Data.fromJson(Map<String, dynamic> json) {
    threats = json['threats'];
    security = json['security'];
    tech = json['tech'];
  }
}

class NotificationListRequest {
  NotificationListRequest({
    this.phone_number,
    this.section_id,
  });
  int? section_id;
  String? phone_number;
}
