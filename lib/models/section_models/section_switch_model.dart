class SectionSwitchSomeAnswer {
  String? message;
  int? status;
  Data? data;

  SectionSwitchSomeAnswer({this.message, this.status, this.data});

  SectionSwitchSomeAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }
}

class Data {
  int? sectionId;
  int? sectionState;

  Data({this.sectionId, this.sectionState});

  Data.fromJson(Map<String, dynamic> json) {
    sectionId = json['section_id'];
    sectionState = json['section_state'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['section_id'] = this.sectionId;
    data['section_state'] = this.sectionState;
    return data;
  }
}
