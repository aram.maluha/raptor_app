import 'dart:convert';

class SectionsNameEdit {
  String? message;
  int? status;
  List<Data>? data;

  SectionsNameEdit({this.message, this.status, this.data});

  SectionsNameEdit.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }
}

class Data {
  String? section_id;
  String? section_label;
  String? section_name;
  String? section_state;
  String? section_date;

  Data(
      {this.section_id,
      this.section_label,
      this.section_name,
      this.section_state,
      this.section_date});

  Data.fromJson(Map<String, dynamic> json) {
    section_id = json['section_id'];
    section_label = json['section_label'];
    section_name = json['section_name'];
    section_state = json['section_state'];
    section_date = json['section_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['section_id'] = this.section_id;
    data['section_label'] = this.section_label;
    data['section_name'] = this.section_name;
    data['section_state'] = this.section_state;
    data['section_date'] = this.section_date;
    return data;
  }
}

class SectionsNameEditRequest {
  SectionsNameEditRequest({
    this.object_id,
    this.phone_number,
    this.section_id,
    this.section_label,
  });
  int? object_id;
  String? phone_number;
  int? section_id;
  String? section_label;
}
