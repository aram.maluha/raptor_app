class SectionsListAnswer {
  String? message;
  int? status;
  List<Data>? data;

  SectionsListAnswer({
    this.message,
    this.status,
    this.data,
  });

  SectionsListAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  int? sectionId;
  int? sectionNumber;
  String? sectionName;
  int? sectionState;
  String? sectionLabel;

  Data({
    this.sectionId,
    this.sectionNumber,
    this.sectionName,
    this.sectionState,
    this.sectionLabel,
  });

  Data.fromJson(Map<String, dynamic> json) {
    sectionId = json["section_id"];
    sectionNumber = json["section_number"];
    sectionLabel = json['section_label'];
    sectionName = json["section_name"];
    sectionState = json["section_state"];
  }
}

class SectionsListRequest {
  SectionsListRequest({
    this.section_id,
    this.organization_id,
    this.phone_number,
  });
  int? section_id;
  int? organization_id;
  String? phone_number;
}
