class AlertButton {
  String? message;
  int? status;
  List<Data>? data;

  AlertButton({this.message, this.status, this.data});

  AlertButton.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] == null) {
      print('я не умер');
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    } else {
      print('я умер');
    }
  }
}

class Data {
  String? buttonId;
  String? buttonName;
  String? buttonState;
  String? buttonDate;

  Data({this.buttonId, this.buttonName, this.buttonState, this.buttonDate});

  Data.fromJson(Map<String, dynamic> json) {
    buttonId = json['button_id'];
    buttonName = json['button_name'];
    buttonState = json['button_state'];
    buttonDate = json['button_date'];
  }
}

class AlertButtonModelRequest {
  AlertButtonModelRequest({
    this.object_id,
    this.phone_number,
  });
  int? object_id;
  String? phone_number;
}
