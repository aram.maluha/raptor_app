class ControlsNameEditAnswer {
  String? message;
  int? status;
  List<Data>? data;

  ControlsNameEditAnswer({this.message, this.status, this.data});

  ControlsNameEditAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }
}

class Data {
  String? control_id;
  String? control_label;
  String? control_name;
  String? control_state;
  String? control_date;

  Data(
      {this.control_id,
      this.control_label,
      this.control_name,
      this.control_state,
      this.control_date});

  Data.fromJson(Map<String, dynamic> json) {
    control_id = json['control_id'];
    control_label = json['control_label'];
    control_name = json['control_name'];
    control_state = json['control_state'];
    control_date = json['control_date'];
  }
}

class ControlsNameEditRequest {
  ControlsNameEditRequest({
    this.phone_number,
    this.object_id,
    this.control_id,
    this.control_label,
  });
  int? object_id;
  String? phone_number;
  int? control_id;
  String? control_label;
}
