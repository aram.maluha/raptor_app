class NotificationSwitchAnswer {
  String? message;
  int? status;
  List<Data>? data;

  NotificationSwitchAnswer({this.message, this.status, this.data});

  NotificationSwitchAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  int? threats;
  int? security;
  int? tech;

  Data({this.threats, this.security, this.tech});

  Data.fromJson(Map<String, dynamic> json) {
    threats = json['threats'];
    security = json['security'];
    tech = json['tech'];
  }
}

class NotificationSwitchRequest {
  NotificationSwitchRequest(
      {this.security,
      this.section_id,
      this.phone_number,
      this.tech,
      this.threats});
  int? section_id;
  String? phone_number;
  String? threats;
  String? security;
  String? tech;
}
