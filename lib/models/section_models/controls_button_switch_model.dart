class SectionSwitchAnswer {
  String? message;
  int? status;
  List<Data>? data;

  SectionSwitchAnswer({this.message, this.status, this.data});

  SectionSwitchAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  int? control_id;
  String? control_label;
  String? control_name;
  bool? control_state;
  String? control_date;

  Data(
      {this.control_date,
      this.control_id,
      this.control_label,
      this.control_name,
      this.control_state});

  Data.fromJson(Map<String, dynamic> json) {
    control_id = json['control_id'];
    control_label = json['control_label'];
    control_name = json['control_name'];
    control_state = json['control_state'];
    control_date = json['control_date'];
  }
}

class SectionSwitchRequest {
  SectionSwitchRequest({
    this.control_id,
    this.control_state,
  });
  int? control_id;
  int? control_state;
}
