class SecurityButtonSwitchAnswer {
  String? message;
  int? status;

  SecurityButtonSwitchAnswer({this.message, this.status});

  SecurityButtonSwitchAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
  }
}

class SecurityButtonSwitchRequest {
  SecurityButtonSwitchRequest({
    this.phone_number,
    this.section_id,
    this.section_state,
  });
  String? phone_number;
  int? section_id;
  int? section_state;
}
