class AlertListAnswer {
  String? message;
  int? status;
  List<Data>? data;

  AlertListAnswer({this.message, this.status, this.data});

  AlertListAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }
}

class Data {
  String? threatId;
  String? threatName;
  String? threatZone;
  String? threatDate;
  String? threatTime;

  Data(
      {this.threatId,
      this.threatName,
      this.threatZone,
      this.threatDate,
      this.threatTime});

  Data.fromJson(Map<String, dynamic> json) {
    threatId = json['threat_id'];
    threatName = json['threat_name'];
    threatZone = json['threat_zone'];
    threatDate = json['threat_date'];
    threatTime = json['threat_time'];
  }
}

class AlertListRequest {
  AlertListRequest({
    this.phone_number,
    this.object_id,
    this.section_number,
    this.date_to,
    this.date_from,
  });
  String? phone_number;
  int? object_id;
  int? section_number;
  DateTime? date_from;
  DateTime? date_to;
}
