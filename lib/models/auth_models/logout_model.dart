import 'dart:convert';

LogOutModel logOutModelFromJson(String str) =>
    LogOutModel.fromJson(json.decode(str));

class LogOutModel {
  LogOutModel({
    required this.message,
    required this.status,
  });

  String message;
  int status;

  factory LogOutModel.fromJson(Map<String, dynamic> json) => LogOutModel(
        message: json["message"],
        status: json["status"],
      );
}

class LogOutAuth {
  LogOutAuth({
    required this.organization_id,
    required this.phone_number,
  });
  String phone_number, organization_id;
}
