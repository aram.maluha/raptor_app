import 'dart:convert';

FirstRegStep firstRegStepFromJson(String str) =>
    FirstRegStep.fromJson(json.decode(str));

class FirstRegStep {
  FirstRegStep({this.message, this.status, this.data});

  String? message;
  int? status;
  String? data;

  factory FirstRegStep.fromJson(Map<String, dynamic> json) => FirstRegStep(
        message: json["message"],
        status: json["status"],
        data: json["data"],
      );
}

class FirstStepReg {
  FirstStepReg({
    required this.prefix,
    required this.phoneNumber,
  });

  String prefix, phoneNumber;
}
