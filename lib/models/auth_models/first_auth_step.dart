class FirstStepAuthAnswer {
  String? message;
  int? status;
  Data? data;

  FirstStepAuthAnswer({this.message, this.status, this.data});

  FirstStepAuthAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = (json['data'] != null ? Data.fromJson(json['data']) : null)!;
  }
}

class Data {
  String? token;
  String? phoneNumber;
  List<Organizations>? organizations;

  Data({this.token, this.phoneNumber, this.organizations});

  Data.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    phoneNumber = json['phone_number'];
    if (json['organizations'] != null) {
      organizations = <Organizations>[];
      json['organizations'].forEach((v) {
        organizations!.add(Organizations.fromJson(v));
      });
    }
  }
}

class Organizations {
  int? organizationId;
  int? organizationNumber;
  String? organizationName;

  Organizations(
      {this.organizationId, this.organizationNumber, this.organizationName});

  Organizations.fromJson(Map<String, dynamic> json) {
    organizationId = json['organization_id'];
    organizationNumber = json['organization_number'];
    organizationName = json['organization_name'];
  }
}

class FirstAuthStepRequest {
  FirstAuthStepRequest(
      {this.prefix, this.phoneNumber, this.password, this.device_token});
  String? prefix, phoneNumber, password, device_token;
}
