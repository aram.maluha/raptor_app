import 'dart:convert';

RegSecondStep regSecondStepFromJson(String str) =>
    RegSecondStep.fromJson(json.decode(str));

class RegSecondStep {
  RegSecondStep(
      {required this.message, required this.status, required this.data});

  String message;
  int status;
  String data;

  factory RegSecondStep.fromJson(Map<String, dynamic> json) => RegSecondStep(
        message: json["message"],
        status: json["status"],
        data: json["data"],
      );
}

class RegStepSecond {
  RegStepSecond({
    required this.verificationCode,
    required this.password,
  });

  String verificationCode, password;
}
