import 'package:intl/intl.dart';

class EventListAnswer {
  String? message;
  int? status;
  List<Data>? data;

  EventListAnswer({this.message, this.status, this.data});

  EventListAnswer.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(Data.fromJson(v));
      });
    }
  }
}

class Data {
  String? eventId;
  String? eventName;
  String? eventUser;
  String? eventDate;
  String? eventTime;

  Data(
      {this.eventId,
      this.eventName,
      this.eventUser,
      this.eventDate,
      this.eventTime});

  Data.fromJson(Map<String, dynamic> json) {
    eventId = json['event_id'];
    eventName = json['event_name'];
    eventUser = json['event_user'];
    eventDate = json['event_date'];
    eventTime = json['event_time'];
  }
}

class EventListRequest {
  EventListRequest({
    this.phone_number,
    this.object_id,
    this.section_number,
    this.date_to,
    this.date_from,
  });
  String? phone_number;
  int? object_id;
  int? section_number;
  DateFormat? date_from;
  DateFormat? date_to;
}
