class UserLocationModel {
  String? message;
  int? status;
  Data? data;

  UserLocationModel({this.message, this.status, this.data});

  UserLocationModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }
}

class Data {
  String? countryName;
  String? countryCode;
  String? ipAddress;

  Data({this.countryName, this.countryCode, this.ipAddress});

  Data.fromJson(Map<String, dynamic> json) {
    countryName = json['country_name'];
    countryCode = json['country_code'];
    ipAddress = json['ip_address'];
  }
}
