class ObjectNameEdit {
  String? message;
  int? status;
  List<Data>? data;

  ObjectNameEdit({this.message, this.status, this.data});

  ObjectNameEdit.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
  }
}

class Data {
  String? objectId;
  String? objectLabel;
  String? objectName;
  String? objectAddress;

  Data({this.objectId, this.objectLabel, this.objectName, this.objectAddress});

  Data.fromJson(Map<String, dynamic> json) {
    objectId = json['object_id'];
    objectLabel = json['object_label'];
    objectName = json['object_name'];
    objectAddress = json['object_address'];
  }
}

class ObjectNameEditRequest {
  ObjectNameEditRequest({
    required this.organization_id,
    required this.phone_number,
    required this.object_id,
    required this.object_label,
  });
  int organization_id;
  String phone_number;
  int object_id;
  String object_label;
}
