import 'dart:convert';

class AlertModel {
  final String? date;
  final String? time;
  final String? zone;
  final String? status;

  AlertModel({this.date, this.time, this.zone, this.status});
}

// AlertModel objectModelFromJson(String str) =>
//     AlertModel.fromJson(json.decode(str));
//
// class AlertModel {
//   AlertModel({
//     required this.message,
//     required this.status,
//     required this.data,
//   });
//
//   String message;
//   int status;
//   List<Data> data;
//
//   factory AlertModel.fromJson(Map<String, dynamic> json) => AlertModel(
//         message: json["message"],
//         status: json["status"],
//         data: List<Data>.from(json["data"].map((x) => Data.fromJson(x))),
//       );
// }
//
// class Data {
//   Data({
//     required this.threatId,
//     required this.threadName,
//     required this.threatZone,
//     required this.threatDate,
//   });
//
//   int threatId;
//   String threadName;
//   int threatZone;
//   DateTime threatDate;
//
//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//         threatId: json["threat_id"],
//         threadName: json["thread_name"],
//         threatZone: json["threat_zone"],
//         threatDate: DateTime.parse(json["threat_date"]),
//       );
// }
//
// class AlertModelRequest {
//   AlertModelRequest({
//     required this.phone_number,
//     required this.object_id,
//     required this.section_num,
//     required this.date_from,
//     required this.date_to,
//   });
//   String phone_number;
//   int object_id;
//   int section_num;
//   DateTime date_from;
//   DateTime date_to;
// }
