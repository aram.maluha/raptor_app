import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

final fStyleText = TextStyle(fontSize: 16.sp, color: Color(0xFF105A89));
final styleText =
    TextStyle(fontSize: 16.sp, color: Colors.black.withOpacity(0.79));
final mainColor = Color(0xFF84CC16);
final hintText = TextStyle(
    fontSize: 12.sp, color: Color(0xFFBCBCBC), fontWeight: FontWeight.w500);

class SectionContainer extends StatelessWidget {
  const SectionContainer(
      {Key? key,
      this.title,
      this.onTap,
      this.iconEdit,
      this.onPressed,
      this.state})
      : super(key: key);

  final String? title;
  final Function? onTap;
  final VoidCallback? iconEdit;
  final VoidCallback? onPressed;
  final int? state;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MaterialButton(
          padding: EdgeInsets.zero,
          onPressed: onPressed,
          child: DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: Colors.white.withOpacity(0.95),
              boxShadow: [
                BoxShadow(
                    color: Colors.black12,
                    blurRadius: 1,
                    spreadRadius: 1,
                    offset: Offset(0.5, 0.5))
              ],
              // border: Border.all(
              //   color: Color(0xFFEA1601),
              // ),
            ),
            child: SizedBox(
              height: 50.w,
              width: 335.w,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 12.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(title!),
                        IconButton(
                          splashRadius: 20,
                          splashColor: mainColor,
                          iconSize: 25.w,
                          constraints: BoxConstraints(),
                          onPressed: iconEdit,
                          icon: Icon(
                            Icons.edit,
                            color: mainColor,
                          ),
                        ),
                      ],
                    ),
                    IconButton(
                        iconSize: 25.w,
                        splashRadius: 20,
                        splashColor: Colors.red,
                        onPressed: onPressed,
                        icon: state == 2
                            ? Icon(
                                Icons.lock,
                                color: Colors.red,
                              )
                            : Icon(
                                Icons.lock_open,
                                color: Colors.green,
                              )),
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 5.w,
        )
      ],
    );
  }
}

class NotificationsManageSwitcher extends StatefulWidget {
  NotificationsManageSwitcher({this.title, this.function, this.light});
  final String? title;
  bool? light = false;
  final VoidCallback? function;

  @override
  _NotificationsManageSwitcherState createState() =>
      _NotificationsManageSwitcherState();
}

class _NotificationsManageSwitcherState
    extends State<NotificationsManageSwitcher> {
  @override
  Widget build(BuildContext context) {
    return MergeSemantics(
      child: Column(
        children: [
          Container(
            height: 45.w,
            child: ListTile(
              title: Row(
                children: [
                  Text(
                    widget.title!,
                    style: TextStyle(fontSize: 14),
                  ),
                ],
              ),
              trailing: CupertinoSwitch(
                trackColor: Colors.grey[500],
                activeColor: mainColor,
                value: widget.light!,
                onChanged: (bool value) {
                  widget.function!();
                  setState(() {
                    widget.light = value;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ManageSwitcher extends StatefulWidget {
  ManageSwitcher({
    Key? key,
    this.title,
    this.light,
    this.function,
    this.iconEdit,
  }) : super(key: key);
  final String? title;
  final VoidCallback? iconEdit;
  bool? light = false;
  final VoidCallback? function;
  @override
  _ManageSwitcherState createState() => _ManageSwitcherState();
}

class _ManageSwitcherState extends State<ManageSwitcher> {
  @override
  Widget build(BuildContext context) {
    return MergeSemantics(
      child: Column(
        children: [
          Container(
            height: 45.w,
            child: ListTile(
              title: Row(
                children: [
                  Text(
                    widget.title!,
                    style: TextStyle(fontSize: 14),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  IconButton(
                    splashRadius: 15,
                    splashColor: mainColor,
                    iconSize: 25.w,
                    constraints: BoxConstraints(),
                    onPressed: widget.iconEdit,
                    icon: Icon(
                      Icons.edit,
                      color: mainColor,
                    ),
                  ),
                ],
              ),
              trailing: CupertinoSwitch(
                trackColor: Colors.grey[500],
                activeColor: mainColor,
                value: widget.light!,
                onChanged: (bool value) {
                  widget.function!();
                  setState(() {
                    widget.light = value;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// class SMSGreyText extends StatelessWidget {
//   SMSGreyText({this.greyText, this.fontSize, this.onPressed});
//
//   final String? greyText;
//   final double? fontSize;
//   final VoidCallback? onPressed;
//
//   @override
//   Widget build(BuildContext context) {
//     return RichText(
//       text: TextSpan(
//           text: greyText,
//           style: TextStyle(
//             fontFamily: 'OpenSans',
//             fontSize: fontSize,
//             color: Color(0xFF828282),
//           ),
//           recognizer: TapGestureRecognizer()
//             ..onTap = () {
//               onPressed!.call();
//             }),
//     );
//   }
// }

class RegLogInButton extends StatelessWidget {
  RegLogInButton(
      {this.paddingLeftRight,
      this.paddingTopBottom,
      this.loginButtonText,
      this.onPressed});

  final double? paddingLeftRight;
  final double? paddingTopBottom;
  final String? loginButtonText;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      padding: EdgeInsets.symmetric(vertical: 10),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5.w),
      ),
      child: MaterialButton(
        padding: EdgeInsets.only(
          left: paddingLeftRight!,
          right: paddingLeftRight!,
          top: paddingTopBottom!,
          bottom: paddingTopBottom!,
        ),
        color: mainColor,
        child: Text(
          loginButtonText!,
          style: TextStyle(
            fontSize: 16.sp,
            color: Colors.white,
          ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}

class RoundedButton extends StatelessWidget {
  final Color? color;
  final String? title;
  final double? height;
  final double? width;
  final VoidCallback? onPressed;

  const RoundedButton(
      {Key? key,
      this.color,
      this.title,
      this.height,
      this.width,
      this.onPressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: MaterialButton(
        // splashColor: Colors.white.withOpacity(0.32),
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        minWidth: width,
        height: height,
        color: color,
        onPressed: onPressed!,
        child: Text(
          title!,
          style: TextStyle(
            color: Colors.white,
            fontSize: 15.sp,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
