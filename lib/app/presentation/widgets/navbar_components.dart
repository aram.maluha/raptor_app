import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/pages/alerts_history.dart';
import 'package:raptor_app/app/presentation/pages/contacts_screen.dart';
import 'package:raptor_app/app/presentation/pages/events_history.dart';
import 'package:raptor_app/app/presentation/pages/notifications_screen.dart';
import 'package:raptor_app/app/presentation/pages/objects_screen.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:raptor_app/controllers/api_controller.dart';

class NavBarFirst extends StatefulWidget {
  const NavBarFirst({Key? key}) : super(key: key);

  @override
  _NavBarFirstState createState() => _NavBarFirstState();
}

class _NavBarFirstState extends State<NavBarFirst> {
  ApiController apiController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10.w),
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: Color(0xFF84CC16),
        ),
        child: SizedBox(
          height: 74.w,
          width: 335.w,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 59.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.w),
                  child: GestureDetector(
                    onTap: () {
                      Get.off(() => ContactsScreen());
                    },
                    child: DecoratedBox(
                      decoration: BoxDecoration(),
                      child: SizedBox(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SvgPicture.asset(
                              'assets/images/icons/call.svg',
                              height: 24.w,
                              width: 24.w,
                            ),
                            Text(
                              'Контакты',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.w),
                  child: GestureDetector(
                    onTap: () {
                      apiController.alertButton = null;
                      apiController.controlsListAnswer = null;
                      Get.off(() => ObjectsScreen());
                    },
                    child: DecoratedBox(
                      decoration: BoxDecoration(),
                      child: SizedBox(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SvgPicture.asset(
                              'assets/images/icons/home.svg',
                              height: 24.w,
                              width: 24.w,
                            ),
                            Text(
                              'Мои объекты',
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class NavBarSecond extends StatefulWidget {
  const NavBarSecond({this.function});

  final VoidCallback? function;

  @override
  _NavBarSecondState createState() => _NavBarSecondState();
}

class _NavBarSecondState extends State<NavBarSecond> {
  ApiController apiController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10.w),
          child: DecoratedBox(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              color: mainColor,
            ),
            child: SizedBox(
              height: 74.w,
              width: 335.w,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(vertical: 16.w),
                      child: GestureDetector(
                        onTap: () {
                          Get.off(() => EventsHistory());
                          widget.function == null ? () {} : widget.function!();
                        },
                        child: DecoratedBox(
                          decoration: BoxDecoration(),
                          child: SizedBox(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/icons/shield_done.svg',
                                  height: 24.w,
                                  width: 24.w,
                                ),
                                Text(
                                  'История событий',
                                  style: TextStyle(
                                      fontSize: 9.sp, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(right: 16.w, top: 16.w, bottom: 16.w),
                      child: GestureDetector(
                        onTap: () {
                          // apiController
                          //     .notificationListModel()
                          //     .then((value) => {
                          //           if (value.status == Status.success)
                          //             {
                          //               Get.off(() => ContactsScreen()),
                          //             }
                          //           else
                          //             {print(value.errorText)}
                          //         });
                          Get.off(() => NotificationsScreen());
                          widget.function == null ? () {} : widget.function!();
                        },
                        child: DecoratedBox(
                          decoration: BoxDecoration(),
                          child: SizedBox(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SvgPicture.asset(
                                  'assets/images/icons/setting.svg',
                                  height: 24.w,
                                  width: 24.w,
                                ),
                                Text(
                                  'Настройки',
                                  style: TextStyle(
                                      fontSize: 9.sp, color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          height: 78.w,
          left: 160.w,
          child: Padding(
            padding: EdgeInsets.only(bottom: 10.w),
            child: GestureDetector(
              onTap: () {
                Get.off(() => AlertsHistory());
                widget.function == null ? () {} : widget.function!();
              },
              child: DecoratedBox(
                decoration: BoxDecoration(),
                child: SizedBox(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      DecoratedBox(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.20),
                              offset: Offset(1, 2),
                              blurRadius: 4,
                              spreadRadius: 1,
                            )
                          ],
                        ),
                        child: CircleAvatar(
                          radius: 25.w,
                          backgroundColor: Colors.white,
                          child: SvgPicture.asset(
                            'assets/images/icons/danger.svg',
                            height: 24.w,
                            width: 24.w,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 5.0),
                        child: Text(
                          'История тревог',
                          style: TextStyle(fontSize: 9.sp, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
