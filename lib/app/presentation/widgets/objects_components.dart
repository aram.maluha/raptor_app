import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ObjectContainer extends StatefulWidget {
  final String? objectName;
  final String? objectAddress;
  final VoidCallback? onPressed;
  final VoidCallback? iconEdit;

  const ObjectContainer({
    Key? key,
    this.objectName,
    this.objectAddress,
    this.onPressed,
    this.iconEdit,
  }) : super(key: key);

  @override
  _ObjectContainerState createState() => _ObjectContainerState();
}

class _ObjectContainerState extends State<ObjectContainer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 24.w),
      child: MaterialButton(
        padding: EdgeInsets.zero,
        onPressed: widget.onPressed,
        child: DecoratedBox(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.white.withOpacity(0.64),
          ),
          child: Stack(children: [
            Column(
              children: [
                SizedBox(
                  height: 124.w,
                  width: 335.w,
                  child: Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 24.w, vertical: 24.w),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 275.w,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    widget.objectName!,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(fontSize: 18.sp),
                                  ),
                                  SizedBox(width: 5.w),
                                  IconButton(
                                    splashRadius: 20,
                                    constraints: BoxConstraints(),
                                    onPressed: widget.iconEdit,
                                    icon: Icon(Icons.edit),
                                    iconSize: 20,
                                  ),
                                ],
                              ),
                              Text(
                                'Адрес: ${widget.objectAddress}',
                                style: TextStyle(
                                    fontSize: 16.sp, color: Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
              left: 290.w,
              top: 40.w,
              child: IconButton(
                padding: EdgeInsets.only(),
                iconSize: 20,
                splashRadius: 20,
                // constraints: BoxConstraints(maxHeight: 20, maxWidth: 20),
                onPressed: widget.onPressed,
                icon: Icon(Icons.arrow_forward_ios_outlined),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
