import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'navbar_components.dart';

class CustomScaffold extends StatefulWidget {
  final Widget? body;
  final String? title;
  final bool? isContactScreen;
  final Widget? icon;
  final Widget? leading;
  final VoidCallback? navBarFunction;

  const CustomScaffold({
    Key? key,
    this.icon,
    this.body,
    this.title,
    this.isContactScreen = true,
    this.navBarFunction,
    this.leading,
  }) : super(key: key);

  @override
  _CustomScaffoldState createState() => _CustomScaffoldState();
}

class _CustomScaffoldState extends State<CustomScaffold> {
  // RefreshController _refreshController =
  //     RefreshController(initialRefresh: false);
  // void _onRefresh() async {
  //   monitor network fetch
  //   await Future.delayed(Duration(milliseconds: 1000));
  // if failed,use refreshFailed()
  // _refreshController.refreshCompleted();
  // }
  //
  // void _onLoading() async {
  //   monitor network fetch
  // await Future.delayed(Duration(milliseconds: 1000));
  // if failed,use loadFailed(),if no data return,use LoadNodata()
  // if (mounted) setState(() {});
  // _refreshController.loadComplete();
  // }

  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/images/main_back.png',
          ),
          fit: BoxFit.fill,
        ),
      ),
      child: Scaffold(
        appBar: AppBar(
          leadingWidth: 70,
          leading: widget.icon,
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(
            widget.title!,
            style: TextStyle(
              fontSize: 24.sp,
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
        body: widget.body,
        // SmartRefresher(
        //   enablePullDown: true,
        //   header: TwoLevelHeader(),
        //   footer: CustomFooter(
        //     builder: (BuildContext context, LoadStatus? status) {
        //       Widget body;
        //       if (status == LoadStatus.idle) {
        //         body = Text("Потяните для загрузки");
        //       } else if (status == LoadStatus.loading) {
        //         body = CupertinoActivityIndicator();
        //       } else if (status == LoadStatus.failed) {
        //         body = Text("Load Failed!Click retry!");
        //       } else if (status == LoadStatus.canLoading) {
        //         body = Text("release to load more");
        //       } else {
        //         body = Text("No more Data");
        //       }
        //       return Container(
        //         height: 100.0,
        //         child: Center(child: body),
        //       );
        //     },
        //   ),
        //   controller: _refreshController,
        //   onRefresh: _onRefresh,
        //   onLoading: _onLoading,
        //   child: widget.body,
        // ),
        bottomNavigationBar: widget.isContactScreen!
            ? NavBarFirst()
            : NavBarSecond(
                function: widget.navBarFunction == null
                    ? () {}
                    : widget.navBarFunction,
              ),
      ),
    );
  }
}
