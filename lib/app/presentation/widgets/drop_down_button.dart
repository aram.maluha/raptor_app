import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/models/auth_models/first_auth_step.dart';
import 'package:raptor_app/models/org_models/org_list_model.dart';

class DropDownButton extends StatefulWidget {
  const DropDownButton({Key? key}) : super(key: key);

  @override
  _DropDownButtonState createState() => _DropDownButtonState();
}

class _DropDownButtonState extends State<DropDownButton> {
  ApiController apiController = Get.find();
  OrganizationsListAnswer? items;
  String? selectedItem;
  @override
  void initState() {
    super.initState();
    items = apiController.organizationsListAnswer;
  }

  // items: items!.data!.organizations!,
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10.w),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 3.w),
        child: SizedBox(
          width: 1.sw,
          child: DropdownButton(
            underline: Divider(
              height: 1.w,
              thickness: 1.w,
              color: Colors.black,
            ),
            hint: Text('Выберите организацию'),
            isExpanded: true,
            value: selectedItem,
            onChanged: (String? string) =>
                setState(() => selectedItem = string),
            items: List<DropdownMenuItem<String>>.generate(
              items!.data!.length,
              (_index) => DropdownMenuItem(
                onTap: () => apiController.organizationId =
                    items!.data![_index].organizationId!,
                value: _index.toString(),
                child: Text(
                  items!.data![_index].organizationName.toString(),
                  overflow: TextOverflow.clip,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
