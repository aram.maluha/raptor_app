import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'main_widgets.dart';
import 'package:get/get.dart';

class ContactsInfo extends StatefulWidget {
  final String? orgName, orgNumber, orgEmail, orgWeb;
  final Widget? orgImage;
  final Widget? contacts;

  const ContactsInfo(
      {Key? key,
      this.orgName,
      this.orgNumber,
      this.orgEmail,
      this.orgWeb,
      this.orgImage,
      this.contacts})
      : super(key: key);

  @override
  _ContactsInfoState createState() => _ContactsInfoState();
}

class _ContactsInfoState extends State<ContactsInfo> {
  ApiController apiController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 20.w),
      child: Column(children: [
        Padding(
          padding: EdgeInsets.fromLTRB(21.w, 23.w, 21.w, 35.w),
          child: Container(
            child: widget.orgImage,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 25.w),
          child: Text(
            '${widget.orgName}',
            style: TextStyle(
                color: Colors.white.withOpacity(0.79), fontSize: 24.sp),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 20.w),
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount:
                  apiController.orgContactsAnswer!.data!.contacts?.length,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: 20.w),
                      child: Container(
                        child: Column(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 5.w),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  // Icon(
                                  //   Icons.info_outline_rounded,
                                  //   color: Color(0xFF105A89),
                                  //   size: 20.w,
                                  // ),
                                  Text(
                                      '  ${apiController.orgContactsAnswer!.data?.contacts![index].type}',
                                      style: fStyleText),
                                ],
                              ),
                            ),
                            Text(
                              '${apiController.orgContactsAnswer!.data?.contacts![index].value}',
                              style: styleText,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              }),
        ),
      ]),
    );
  }
}
