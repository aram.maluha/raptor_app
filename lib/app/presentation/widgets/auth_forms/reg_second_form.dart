import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/models/auth_models/first_auth_step.dart';
import 'package:raptor_app/models/auth_models/second_reg_step.dart';
import 'package:raptor_app/services/response_result.dart';

import '../main_widgets.dart';

class RegSecondForm extends StatefulWidget {
  RegSecondForm();

  @override
  _RegSecondFormState createState() => _RegSecondFormState();
}

class _RegSecondFormState extends State<RegSecondForm> {
  ApiController apiController = Get.find();
  TextEditingController _controllerSms = TextEditingController();
  TextEditingController passController = TextEditingController();
  TextEditingController passConfirmController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: Colors.transparent,
      child: SizedBox(
        height: 400.w,
        child: Stack(
          children: [
            DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
              ),
              child: SizedBox(
                width: 295.w,
                height: 381.w,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 25.w,
                    right: 25.w,
                    top: 65.w,
                  ),
                  child: Form(
                    key: formKey,
                    child: Column(
                      children: [
                        Text(
                          'Придумайте пароль',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.sp,
                              fontWeight: FontWeight.w500),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 8.w, top: 24.w),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  SvgPicture.asset(
                                    'assets/images/icons/lock.svg',
                                    height: 14.w,
                                    color: mainColor,
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(
                                    'Пароль',
                                    style: hintText,
                                  )
                                ],
                              ),
                              TextFormField(
                                controller: passController,
                                style: TextStyle(
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                                obscureText: true,
                                maxLength: 20,
                                maxLengthEnforcement:
                                    MaxLengthEnforcement.enforced,
                                validator: (value) =>
                                    validatePassField(value!, 6),
                                decoration: InputDecoration(
                                  isCollapsed: true,
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFFBCBCBC),
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  errorBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.redAccent,
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.only(
                                      top: 15.w, left: 20.w, bottom: 5.w),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 8.w),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  SvgPicture.asset(
                                    'assets/images/icons/lock.svg',
                                    height: 14.w,
                                    color: mainColor,
                                  ),
                                  SizedBox(
                                    width: 5.w,
                                  ),
                                  Text(
                                    'Повторите пароль',
                                    style: hintText,
                                  )
                                ],
                              ),
                              TextFormField(
                                controller: passConfirmController,
                                style: TextStyle(
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500),
                                obscureText: true,
                                maxLength: 20,
                                maxLengthEnforcement:
                                    MaxLengthEnforcement.enforced,
                                validator: (value) =>
                                    validatePassField(value!, 6),
                                decoration: InputDecoration(
                                  isCollapsed: true,
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Color(0xFFBCBCBC),
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Colors.lightBlue,
                                    ),
                                  ),
                                  errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                    color: Colors.redAccent,
                                  )),
                                  contentPadding: EdgeInsets.only(
                                      top: 15.w, left: 20.w, bottom: 5.w),
                                ),
                              ),
                              Column(
                                children: [
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.sms,
                                        color: mainColor,
                                        size: 15.w,
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      Text(
                                        'Введите SMS-код',
                                        style: hintText,
                                      ),
                                    ],
                                  ),
                                  // SizedBox(height: 10.w),
                                  TextFormField(
                                    controller: _controllerSms,
                                    inputFormatters: [
                                      // LengthLimitingTextInputFormatter(4)
                                    ],
                                    keyboardType: TextInputType.number,
                                    decoration: InputDecoration(
                                      hintText: '_ _ _ _',
                                      isCollapsed: true,
                                      contentPadding: EdgeInsets.only(
                                        top: 15.w,
                                        left: 20.w,
                                        bottom: 5.w,
                                      ),
                                    ),
                                    maxLength: 4,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 359.w,
              left: 61.w,
              right: 61.w,
              child: ArgonButton(
                height: 44.w,
                minWidth: 193.w,
                roundLoadingShape: false,
                width: MediaQuery.of(context).size.width * 0.45,
                onTap: (startLoading, stopLoading, btnState) {
                  if (btnState == ButtonState.Idle) {
                    startLoading();
                    if (formKey.currentState!.validate()) {}
                    postData().then((value) => stopLoading());
                  }
                },
                child: Text(
                  "Зарегистрироваться",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                ),
                loader: Container(
                  padding: EdgeInsets.all(10),
                  child: SpinKitRing(
                    color: Colors.white,
                    size: 60.w,
                    lineWidth: 3.w,
                  ),
                ),
                borderRadius: 5.0,
                color: mainColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
  //ClipRRect(
  //                 borderRadius: BorderRadius.circular(5),
  //                 child: MaterialButton(
  //                   materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
  //                   minWidth: 193.w,
  //                   height: 44.w,
  //                   color: mainColor,
  //                   onPressed: () {
  //                     if (formKey.currentState!.validate()) {
  //                       postData();
  //                     }
  //                     //navigation HERE
  //                   },
  //                   child: Text(
  //                     'Зарегистрироваться',
  //                     style: TextStyle(
  //                       color: Colors.white,
  //                       fontSize: 15.sp,
  //                       fontWeight: FontWeight.w500,
  //                     ),
  //                   ),
  //                 ),
  //               ),

  String? validatePassField(String value, int? minLength) {
    if (passController.text != passConfirmController.text) {
      return 'Пароли не совпадают';
    }
    if (value.isEmpty) {
      return 'Вы не заполнили поле';
    }
    if (minLength != null) if (value.length < minLength)
      return 'Минимум $minLength цифр';
    return null;
  }

  Future postData() async {
    RegStepSecond secondRegStep = RegStepSecond(
      verificationCode: _controllerSms.text,
      password: passController.text,
    );
    await apiController.secondRegStep(secondRegStep).then((value) {
      if (value.status == Status.success) {
        // postData() {
        // }
        Get.snackbar(
            'Регистрация прошла успешно!', 'Теперь пройдите авторизацию',
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.green,
            colorText: Colors.white,
            animationDuration: 1.seconds,
            duration: 3.seconds);
      } else {
        Get.snackbar('Ошибка', value.errorText.toString(),
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.redAccent,
            colorText: Colors.white,
            animationDuration: 1.seconds,
            duration: 3.seconds);
      }
    });
  }
}
