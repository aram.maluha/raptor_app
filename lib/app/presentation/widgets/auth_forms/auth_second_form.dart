import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/pages/objects_screen.dart';
import 'package:raptor_app/app/presentation/widgets/drop_down_button.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/services/response_result.dart';

class AuthSecondForm extends StatefulWidget {
  const AuthSecondForm();

  @override
  _AuthSecondFormState createState() => _AuthSecondFormState();
}

class _AuthSecondFormState extends State<AuthSecondForm> {
  ApiController apiController = Get.find();
  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: Colors.transparent,
      child: SizedBox(
        height: 400.w,
        child: Stack(
          children: [
            DecoratedBox(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
              ),
              child: SizedBox(
                height: 178.w,
                width: 295.w,
                child: Column(
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(left: 31.w, top: 60.w, right: 31.w),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              SvgPicture.asset(
                                'assets/images/icons/paper.svg',
                                color: mainColor,
                                height: 14.w,
                              ),
                              SizedBox(
                                width: 6.w,
                              ),
                              Text(
                                'Выбор организации',
                                style: TextStyle(
                                    color: Color(0xFFBCBCBC),
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w500),
                              )
                            ],
                          ),
                          DropDownButton(),
                          //  DROPDOWN_SEARCH HERE
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
              left: 62.w,
              right: 62.w,
              top: 156.w,
              child: ArgonButton(
                minWidth: 193.w,
                height: 44.w,
                roundLoadingShape: false,
                width: MediaQuery.of(context).size.width * 0.45,
                onTap: (startLoading, stopLoading, btnState) {
                  if (btnState == ButtonState.Idle) {
                    startLoading();
                    apiController
                        .organizationsChoose(apiController.organizationId!)
                        .then(
                          (value) => {
                            if (value.status == Status.success)
                              {
                                Get.offAll(
                                  () => ObjectsScreen(),
                                ),
                              }
                            else
                              {
                                Get.snackbar(
                                  'Ошибка',
                                  value.errorText.toString(),
                                  backgroundColor: Colors.red,
                                  colorText: Colors.white,
                                ),
                              },
                            stopLoading(),
                          },
                        );
                  }
                },
                child: Text(
                  "Далее",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                ),
                loader: Container(
                  padding: EdgeInsets.all(10),
                  child: SpinKitRing(
                    color: Colors.white,
                    size: 60.w,
                    lineWidth: 3.w,
                  ),
                ),
                borderRadius: 5.0,
                color: mainColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
