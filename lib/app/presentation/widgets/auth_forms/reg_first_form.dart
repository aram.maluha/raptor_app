import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/widgets/auth_forms/reg_second_form.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/models/auth_models/first_reg_step.dart';
import 'package:raptor_app/services/response_result.dart';

import '../main_widgets.dart';

class RegFirstForm extends StatefulWidget {
  RegFirstForm();

  @override
  _RegFirstFormState createState() => _RegFirstFormState();
}

class _RegFirstFormState extends State<RegFirstForm> {
  bool isRegFirstForm = true;
  PageController controller = PageController();
  ApiController apiController = Get.find();
  TextEditingController phoneController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: controller,
      physics: NeverScrollableScrollPhysics(),
      children: [
        ColoredBox(
          color: Colors.transparent,
          child: SizedBox(
            height: 400.w,
            child: Stack(
              children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                  ),
                  child: SizedBox(
                    height: 178.w,
                    width: 295.w,
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: 31.w, top: 60.w, right: 31.w),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Icon(
                                    Icons.call,
                                    color: mainColor,
                                    size: 15.w,
                                  ),
                                  Text(
                                    ' Телефон',
                                    style: TextStyle(
                                        color: Color(0xFFBCBCBC),
                                        fontSize: 12.sp,
                                        fontWeight: FontWeight.w500),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Stack(
                                    children: [
                                      Positioned(
                                        left: 10,
                                        child: SizedBox(
                                          width: 40.w,
                                          child: TextFormField(
                                            expands: false,
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.zero),
                                          ),
                                        ),
                                      ),
                                      Positioned(
                                        child: CountryCodePicker(
                                          onChanged: (value) {
                                            apiController.prefix =
                                                value.toString();
                                          },
                                          boxDecoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            color: Colors.white,
                                          ),
                                          showFlagMain: false,
                                          padding: EdgeInsets.zero,
                                          // showDropDownButton: true,
                                          dialogSize: Size.fromHeight(450.w),
                                          // initialSelection: 'KZ',
                                          favorite: [
                                            '+7',
                                            'KZ',
                                            '+7',
                                            'RU',
                                            '+380',
                                            'UA',
                                            '+996',
                                            'KG',
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  // SizedBox(width: 20.w),
                                  SizedBox(
                                    width: 170.w,
                                    child: TextFormField(
                                      style: TextStyle(
                                          fontSize: 14.sp, color: Colors.black),
                                      keyboardType: TextInputType.number,
                                      controller: phoneController,
                                      inputFormatters: [
                                        FilteringTextInputFormatter.digitsOnly,
                                        FilteringTextInputFormatter(
                                            RegExp(r'^[()\d -]{1,15}$'),
                                            allow: true),
                                      ],
                                      decoration: InputDecoration(
                                        contentPadding: EdgeInsets.zero,
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.never,
                                        hintText: ' Номер телефона',
                                        labelStyle: TextStyle(
                                            fontSize: 14.sp,
                                            color:
                                                Colors.black.withOpacity(0.64)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 61.w,
                  top: 156.w,
                  right: 61.w,
                  child: ArgonButton(
                    height: 44.w,
                    minWidth: 193.w,
                    roundLoadingShape: false,
                    width: MediaQuery.of(context).size.width * 0.45,
                    onTap: (startLoading, stopLoading, btnState) {
                      if (btnState == ButtonState.Idle) {
                        startLoading();
                        postData().then((value) => stopLoading());
                      }
                    },
                    child: Text(
                      "Далее",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                    ),
                    loader: Container(
                      padding: EdgeInsets.all(10),
                      child: SpinKitRing(
                        color: Colors.white,
                        size: 60.w,
                        lineWidth: 3.w,
                      ),
                    ),
                    borderRadius: 5.0,
                    color: mainColor,
                  ),
                  // ClipRRect(
                  //   borderRadius: BorderRadius.circular(5),
                  //   child: MaterialButton(
                  //     materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  //     minWidth: 193.w,
                  //     height: 44.w,
                  //     color: mainColor,
                  //     onPressed: () {
                  //       postData();
                  //     },
                  //     child: Text(
                  //       'Далее',
                  //       style: TextStyle(
                  //         color: Colors.white,
                  //         fontSize: 15.sp,
                  //         fontWeight: FontWeight.w500,
                  //       ),
                  //     ),
                  //   ),
                  // ),
                ),
              ],
            ),
          ),
        ),
        RegSecondForm(),
      ],
    );
  }

  Future postData() async {
    FirstStepReg firstStepReg = FirstStepReg(
      prefix: apiController.prefix,
      phoneNumber: phoneController.text,
    );
    await apiController.firstRegStep(firstStepReg).then((value) {
      if (value.status == Status.success) {
        Get.snackbar(
          'Авторизация прошла успешно!',
          'SMS-код придет на введенный Вами номер',
          duration: 4.seconds,
          colorText: Colors.white,
          backgroundColor: Colors.green,
        );
        setState(() {
          isRegFirstForm = true;
          controller.animateToPage(1,
              duration: 400.milliseconds, curve: Curves.easeInSine);
        });
      } else {
        Get.snackbar(
          'Ошибка',
          value.errorText.toString(),
          snackPosition: SnackPosition.TOP,
          backgroundColor: Colors.redAccent,
          barBlur: 10,
          animationDuration: 1.seconds,
          duration: 3.seconds,
        );
      }
    });
  }
}
// VoidCallback? _submitForm() {
//   if (_formKey.currentState!.validate()) {
//     print('phone_number: ${_phoneNumberController.text}');
//   }
// }
// void _submitFormTwo() {
//   if (_formKey.currentState!.validate()) {
//     print('password: ${_passwordController.text}');
//   }
// }

// String? _validatePassword(String? value) {
//   if (_passwordController.text.length != 8) {
//     return 'Не менее 8 символов';
//   } else if (_passwordConfirmController.text != _passwordController.text) {
//     return 'Пароли не совпадают';
//   } else {
//     return null;
//   }
// }
//
// bool _validatePhoneNumber(String input) {
//   final _phoneExp = RegExp(r'^(?:[+0][1-9])?[0-9]{11}$');
//   return _phoneExp.hasMatch(input);
// }
//
// final _phoneNumberController = TextEditingController();
// final _passwordController = TextEditingController();
// final _passwordConfirmController = TextEditingController();
//
// @override
// void dispose() {
//   _phoneNumberController.dispose();
//   _passwordController.dispose();
//   _passwordConfirmController.dispose();
//   super.dispose();
// }
