import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/widgets/auth_forms/auth_second_form.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/models/auth_models/first_auth_step.dart';
import 'package:raptor_app/services/response_result.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthFirstForm extends StatefulWidget {
  AuthFirstForm();

  @override
  _AuthFirstFormState createState() => _AuthFirstFormState();
}

class _AuthFirstFormState extends State<AuthFirstForm> {
  late SharedPreferences prefs;
  bool isAuthFirstForm = true;
  PageController controller = PageController();
  ApiController apiController = Get.find();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passController = TextEditingController();
  String prefixCode = '';

  @override
  void initState() {
    phoneController.text = '7012235700';
    passController.text = '12345678';
    apiController.userLocationModel?.data?.countryCode = prefixCode;
    super.initState();
  }

  // void _onCountryChange(CountryCode countryCode) {
  //   apiController.userLocationModel?.data?.countryCode = countryCode as String?;
  //   print('code: ' + countryCode.toString());
  // }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: controller,
      physics: NeverScrollableScrollPhysics(),
      children: [
        ColoredBox(
          color: Colors.transparent,
          child: SizedBox(
            height: 400.w,
            child: Stack(
              children: [
                DecoratedBox(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                  ),
                  child: SizedBox(
                    width: 295.w,
                    height: 306.w,
                    child: Padding(
                      padding:
                          EdgeInsets.only(right: 25.w, left: 25.w, top: 62.w),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              SvgPicture.asset(
                                'assets/images/icons/call_number.svg',
                                height: 12.w,
                                color: mainColor,
                              ),
                              Text(
                                '  Телефон',
                                style: hintText,
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Stack(
                                children: [
                                  Positioned(
                                    left: 10,
                                    child: SizedBox(
                                      width: 40.w,
                                      child: TextFormField(
                                        expands: false,
                                        decoration: InputDecoration(
                                            contentPadding: EdgeInsets.zero),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    child: CountryCodePicker(
                                      // onInit: (value) => apiController
                                      //     .userLocationModel
                                      //     ?.data
                                      //     ?.countryCode = value.toString(),
                                      onChanged: (value) =>
                                          prefixCode = value.toString(),
                                      // {
                                      //   apiController.prefix = value.toString();
                                      // },
                                      boxDecoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.white,
                                      ),
                                      showFlagMain: false,
                                      padding: EdgeInsets.zero,
                                      // showDropDownButton: true,
                                      dialogSize: Size.fromHeight(450.w),
                                      // initialSelection: '',
                                      favorite: [
                                        '+7',
                                        'KZ',
                                        '+7',
                                        'RU',
                                        '+380',
                                        'UA',
                                        '+996',
                                        'KG',
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              // SizedBox(width: 20.w),
                              SizedBox(
                                width: 175.w,
                                child: TextFormField(
                                  style: TextStyle(
                                      fontSize: 14.sp, color: Colors.black),
                                  keyboardType: TextInputType.number,
                                  controller: phoneController,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly,
                                    FilteringTextInputFormatter(
                                        RegExp(r'^[()\d -]{1,15}$'),
                                        allow: true),
                                  ],
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.zero,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    hintText: ' Номер телефона',
                                    labelStyle: TextStyle(
                                        fontSize: 14.sp,
                                        color: Colors.black.withOpacity(0.64)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20.w,
                          ),
                          Column(
                            children: [
                              Row(
                                children: [
                                  SvgPicture.asset(
                                    'assets/images/icons/lock.svg',
                                    height: 12.w,
                                    color: mainColor,
                                  ),
                                  Text(
                                    '  Пароль',
                                    style: hintText,
                                  )
                                ],
                              ),
                              SizedBox(
                                width: 240,
                                child: TextFormField(
                                  controller: passController,
                                  style: TextStyle(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w500),
                                  obscureText: true,
                                  maxLength: 20,
                                  maxLengthEnforcement:
                                      MaxLengthEnforcement.none,
                                  decoration: InputDecoration(
                                    isCollapsed: true,
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFBCBCBC),
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.lightBlue,
                                      ),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                      color: Colors.redAccent,
                                    )),
                                    contentPadding: EdgeInsets.only(
                                        top: 15.w, left: 17.w, bottom: 10.w),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          // ClipRRect(
                          //   // clipBehavior: Clip.antiAlias,
                          //   borderRadius: BorderRadius.circular(40),
                          //   child: IconButton(
                          //       onPressed: () {},
                          //       icon: Icon(
                          //         Icons.fingerprint,
                          //         size: 25.w,
                          //       )),
                          // ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              TextButton(
                                onPressed: () {
                                  Get.snackbar(
                                    'Забыли пароль?',
                                    'Для сброса пароля перейдите во вкладку "Регистрация" и пройдите регистрацию заново',
                                    colorText: Colors.white,
                                    backgroundColor: Colors.blue,
                                    duration: 6.seconds,
                                    animationDuration: 400.milliseconds,
                                  );
                                },
                                style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero,
                                ),
                                child: Text(
                                  'Забыли пароль?',
                                  style: TextStyle(
                                      color: mainColor,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.sp),
                                ),
                              ),
                              // TextButton(
                              //   onPressed: () {
                              //     Get.to(() => BottomSheetFingerprint());
                              //   },
                              //   child: Text('SHEET'),
                              // ),
                              // TextButton(
                              //   onPressed: () {
                              //     Get.to(() => BioAuthScreen());
                              //   },
                              //   child: Text(
                              //     'Fingerprint',
                              //     style:
                              //         TextStyle(fontSize: 14, color: mainColor),
                              //   ),
                              // ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: 284.w,
                  left: 62.w,
                  right: 62.w,
                  child: ArgonButton(
                    height: 44.w,
                    minWidth: 193.w,
                    roundLoadingShape: false,
                    width: MediaQuery.of(context).size.width * 0.45,
                    onTap: (startLoading, stopLoading, btnState) {
                      if (btnState == ButtonState.Idle) {
                        startLoading();
                        postData().then((value) => stopLoading());
                      }
                    },
                    child: Text(
                      "Далее",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                    ),
                    loader: Container(
                      padding: EdgeInsets.all(10),
                      child: SpinKitRing(
                        color: Colors.white,
                        size: 60.w,
                        lineWidth: 3.w,
                      ),
                    ),
                    borderRadius: 5.0,
                    color: mainColor,
                  ),
                ),
              ],
            ),
          ),
        ),
        AuthSecondForm(),
      ],
    );
  }

  String? validatePassField(String value, int? minLength) {
    if (value.isEmpty) {
      return 'Поле заполнено неверно';
    }
    if (minLength != null) if (value.length < minLength)
      return 'Минимум $minLength цифр';
    return null;
  }

  Future postData() async {
    FirstAuthStepRequest firstAuthStepRequest = FirstAuthStepRequest(
        prefix: apiController.prefix = prefixCode,
        phoneNumber: phoneController.text,
        password: passController.text);

    print([
      firstAuthStepRequest.prefix,
      firstAuthStepRequest.phoneNumber,
      firstAuthStepRequest.password
    ]);
    await apiController.firstStepAuth(firstAuthStepRequest).then((value) {
      if (value.status == Status.success) {
        Get.snackbar('Авторизация прошла успешно', 'Выберите организацию',
            backgroundColor: Colors.green,
            colorText: Colors.white,
            duration: 4.seconds);
        setState(() {
          isAuthFirstForm = true;
          controller.animateToPage(1,
              duration: Duration(milliseconds: 300), curve: Curves.easeInSine);
        });
      } else {
        Get.snackbar('Ошибка', value.errorText.toString(),
            backgroundColor: Colors.redAccent,
            colorText: Colors.white,
            duration: 4.seconds);
      }
    });
  }
}
