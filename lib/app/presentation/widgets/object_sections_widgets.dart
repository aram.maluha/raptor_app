import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:get/get.dart';
import 'package:raptor_app/services/response_result.dart';

class SosButton extends StatefulWidget {
  const SosButton(this.onPressed);
  final VoidCallback onPressed;

  @override
  _SosButtonState createState() => _SosButtonState();
}

class _SosButtonState extends State<SosButton> {
  ApiController apiController = Get.find();
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: Padding(
        padding: EdgeInsets.only(top: 10.w, bottom: 30.w),
        child: ArgonTimerButton(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          height: 44.w,
          width: 335.w,
          // width: MediaQuery.of(context).size.width * 0.45,
          minWidth: MediaQuery.of(context).size.width * 10,
          color: Color(0xFFEA1601),
          roundLoadingShape: false,
          disabledColor: Colors.black12,
          borderRadius: 4,
          onTap: (startTimer, btnState) {
            if (btnState == ButtonState.Idle) {
              apiController.getAlertButton().then((value) {
                if (value.status == Status.success) {
                  // if (DateTime.now()  DateTime.now().difference(DateTime.parse('2021-09-20 11:52:25'))) {

                  // }
                  // DateTime.now() > DateTime.now()
                  // .difference(DateTime.parse('2021-09-20 11:52:25'))
                  // print(DateTime.now());
                  // Get.snackbar(apiController.alertButton!.message!,
                  //     'Кнопка заблокирована на 30 секунд',
                  //     backgroundColor: Colors.green,
                  //     colorText: Colors.white,
                  //     duration: 5.seconds);
                } else {
                  Get.snackbar('Error', value.errorText.toString());
                }
              });
              startTimer(30);
            }
          },
          child: SizedBox(
            width: 335.w,
            child: Text(
              'SOS',
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.center,
            ),
          ),
          loader: (timeLeft) {
            return Text(
              'Отправить повторно через: $timeLeft',
              style: TextStyle(color: Colors.white, fontSize: 16.sp),
              textAlign: TextAlign.center,
            );
          },
        ),
      ),
    );
  }
}
// MaterialButton(
//   shape: RoundedRectangleBorder(
//     borderRadius: BorderRadius.all(
//       Radius.circular(5),
//     ),
//   ),
//   color: Color(0xFFEA1601),
//   onPressed: widget.onPressed,
//   height: 44.w,
//   minWidth: 335.w,
//   child: Text(
//     'SOS',
//     style: TextStyle(fontSize: 16.sp, color: Colors.white),
//   ),
// ),
//
//
//
//

class ObjectSectionContainer extends StatefulWidget {
  final String? objectName;
  final String? objectAddress;

  const ObjectSectionContainer({Key? key, this.objectName, this.objectAddress})
      : super(key: key);

  @override
  _ObjectSectionContainerState createState() => _ObjectSectionContainerState();
}

class _ObjectSectionContainerState extends State<ObjectSectionContainer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 8.w),
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: Colors.white.withOpacity(0.64),
        ),
        child: SizedBox(
          height: 104.w,
          width: 335.w,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 16.w, horizontal: 19.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      widget.objectName!,
                      style: TextStyle(fontSize: 20.sp),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.w,
                ),
                Text(
                  widget.objectAddress!,
                  style: TextStyle(fontSize: 16.sp),
                  softWrap: true,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
