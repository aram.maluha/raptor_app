import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/widgets/custom_scaffold.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/models/section_models/notification_switch.dart';

class NotificationsScreen extends StatefulWidget {
  NotificationsScreen({Key? key}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  ApiController apiController = Get.find();
  String getSectionName() {
    var answer = apiController.sectionsListAnswer!.data!
        .firstWhere((e) => e.sectionNumber == apiController.sectionNumber);
    String name = answer.sectionName!;
    return name;
  }

  @override
  void initState() {
    apiController.notificationListModel();
    super.initState();
    // apiController.notificationSwitchModel(NotificationSwitchRequest());
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      title: 'Настройки уведомлений',
      isContactScreen: false,
      body: GetBuilder<ApiController>(
        builder: (_) => apiController.loading &&
                apiController.notificationListAnswer?.data == null
            ? Center(
                child: CircleAvatar(
                    radius: 35.w,
                    backgroundColor: Colors.white.withOpacity(0.64),
                    child: Container(
                      padding: EdgeInsets.all(10.w),
                      height: 75.w,
                      width: 75.w,
                      child: CircularProgressIndicator(
                        color: Colors.blueAccent,
                      ),
                      decoration: BoxDecoration(),
                    )))
            : apiController.notificationListAnswer?.data != null
                ? Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white.withOpacity(0.64),
                        ),
                        height: 85.w,
                        width: 335.w,
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 19.w, vertical: 16.w),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    '${getSectionName()}',
                                    style: TextStyle(
                                      fontSize: 20.sp,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 5.w,
                              ),
                              Row(
                                children: [
                                  Text(
                                    'Основной (все)',
                                    style: TextStyle(
                                      fontSize: 17.sp,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20.w,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 19.w),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white.withOpacity(0.64),
                          ),
                          child: SizedBox(
                            // height: 150.w,
                            width: 335.w,
                            child: Column(
                              children: [
                                NotificationsManageSwitcher(
                                  function: () {
                                    apiController.notificationSwitchModel(
                                      NotificationSwitchRequest(
                                        tech: apiController
                                            .notificationListAnswer
                                            ?.data?[0]
                                            .tech,
                                        threats: apiController
                                                    .notificationListAnswer!
                                                    .data![0]
                                                    .threats ==
                                                '0'
                                            ? '1'
                                            : '0',
                                        security: apiController
                                            .notificationListAnswer
                                            ?.data?[0]
                                            .security,
                                      ),
                                    );
                                    apiController.update();
                                  },
                                  title: 'Тревожные',
                                  light: apiController.notificationListAnswer!
                                              .data?[0].threats ==
                                          '0'
                                      ? false
                                      : true,
                                ),
                                SizedBox(
                                  height: 7.w,
                                ),
                                DecoratedBox(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.white)),
                                  child: SizedBox(
                                    width: 335.w,
                                    height: 1.w,
                                  ),
                                ),
                                NotificationsManageSwitcher(
                                  function: () {
                                    apiController.notificationSwitchModel(
                                      NotificationSwitchRequest(
                                          tech: apiController
                                              .notificationListAnswer!
                                              .data?[0]
                                              .tech,
                                          threats: apiController
                                              .notificationListAnswer!
                                              .data?[0]
                                              .threats,
                                          security: apiController
                                                      .notificationListAnswer!
                                                      .data![0]
                                                      .security ==
                                                  '0'
                                              ? '1'
                                              : '0'),
                                    );
                                    // apiController.update();
                                  },
                                  title: 'Охранные',
                                  light: apiController.notificationListAnswer!
                                              .data?[0].security ==
                                          '0'
                                      ? false
                                      : true,
                                ),
                                SizedBox(
                                  height: 7.w,
                                ),
                                DecoratedBox(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.white)),
                                  child: SizedBox(
                                    width: 335.w,
                                    height: 1.w,
                                  ),
                                ),
                                NotificationsManageSwitcher(
                                  function: () {
                                    apiController.notificationSwitchModel(
                                      NotificationSwitchRequest(
                                          tech: apiController
                                                      .notificationListAnswer!
                                                      .data![0]
                                                      .tech ==
                                                  '0'
                                              ? '1'
                                              : '0',
                                          security: apiController
                                              .notificationListAnswer!
                                              .data?[0]
                                              .security,
                                          threats: apiController
                                              .notificationListAnswer!
                                              .data?[0]
                                              .threats),
                                    );
                                    // apiController.update();
                                  },
                                  title: 'Технические',
                                  light: apiController.notificationListAnswer!
                                              .data?[0].tech ==
                                          '0'
                                      ? false
                                      : true,
                                ),
                                SizedBox(
                                  height: 7.w,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : SizedBox(),
      ),
    );
  }
}
