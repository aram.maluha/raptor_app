import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/widgets/custom_scaffold.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/services/response_result.dart';

class SecurityScreen extends StatefulWidget {
  const SecurityScreen({Key? key}) : super(key: key);

  @override
  _SecurityScreenState createState() => _SecurityScreenState();
}

class _SecurityScreenState extends State<SecurityScreen> {
  int? isPressed = 0;
  String? title = '';
  ApiController apiController = Get.find();
  @override
  void initState() {
    isPressed = apiController.isPressed;
    title = Get.arguments[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ApiController>(
      builder: (_) => CustomScaffold(
        isContactScreen: false,
        title: title,
        body: Padding(
          padding: EdgeInsets.only(left: 20.w, right: 20.w, top: 36.w),
          child: Column(
            children: [
              SizedBox(
                width: 360.w,
                height: 50.w,
                child: Center(
                  child: isPressed == 2
                      ? Text(
                          'Снять с охраны',
                          style:
                              TextStyle(fontSize: 31.sp, color: Colors.white),
                        )
                      : Text(
                          'Поставить на охрану',
                          style:
                              TextStyle(fontSize: 31.sp, color: Colors.white),
                        ),
                ),
              ),
              Center(
                child: Padding(
                  padding: EdgeInsets.only(top: 77.w),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(90),
                    child: Stack(
                      children: [
                        CircleAvatar(
                            radius: 75.w,
                            backgroundColor: Colors.white.withOpacity(0.64),
                            child: apiController.loading == true
                                ? Container(
                                    padding: EdgeInsets.all(10.w),
                                    height: 130.w,
                                    width: 130.w,
                                    child: CircularProgressIndicator(
                                      color: Colors.blueAccent,
                                    ),
                                    decoration: BoxDecoration(),
                                  )
                                : SizedBox()),
                        Positioned(
                          top: 40.w,
                          left: 40.w,
                          child: IconButton(
                            iconSize: 55.w,
                            color: isPressed == 2 ? Colors.red : Colors.green,
                            icon: Icon(isPressed == 2
                                ? Icons.lock_rounded
                                : Icons.lock_open_rounded),
                            splashColor: Colors.white.withOpacity(0.64),
                            onPressed: () {
                              unawaited(apiController
                                  .securityButtonModel(isPressed == 2 ? 1 : 2)
                                  .then((value) {
                                if (value.status == Status.success) {
                                  isPressed = apiController
                                      .sectionSwitchSomeAnswer!
                                      .data!
                                      .sectionState!;
                                  apiController
                                      .sectionsModel()
                                      .then((value) => apiController.update());
                                } else
                                  // dispose();
                                  Get.snackbar(
                                      'Ошибка', value.errorText.toString(),
                                      backgroundColor: Colors.red,
                                      colorText: Colors.white,
                                      duration: 2.seconds,
                                      animationDuration: 250.milliseconds);
                                // (print(value.errorText.toString()));
                              }));
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<Result> press() async {
    var result;
    unawaited(apiController
        .securityButtonModel(isPressed == 2 ? 1 : 2)
        .then((value) => result = value));
    return result;
  }
}
