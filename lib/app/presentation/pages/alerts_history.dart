import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:raptor_app/app/presentation/widgets/custom_scaffold.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/models/alert_model.dart';
import 'package:raptor_app/services/response_result.dart';

List<TableRow> table = [
  TableRow(
    decoration: BoxDecoration(
        color: Color(0xFF84CC16), borderRadius: BorderRadius.circular(5)),
    children: [
      TableRowItem(
        text: 'Дата',
      ),
      TableRowItem(
        text: 'Время',
      ),
      TableRowItem(
        text: 'Зона',
      ),
      TableRowItem(
        text: 'Статус нарушения',
      ),
    ],
  ),
];

class AlertsHistory extends StatefulWidget {
  const AlertsHistory({Key? key}) : super(key: key);

  @override
  _AlertsHistoryState createState() => _AlertsHistoryState();
}

class _AlertsHistoryState extends State<AlertsHistory> {
  ApiController apiController = Get.find();
  DateTime firstSelectedDate = DateTime.now();
  DateTime secondSelectedDate = DateTime.now();

  Future<void> _firstSelectDate(BuildContext context) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: firstSelectedDate,
      firstDate: DateTime(2018),
      lastDate: DateTime.now(),
    );
    picked = picked!.toLocal();

    if (picked != firstSelectedDate)
      setState(() {
        firstSelectedDate = picked!;
      });
  }

  Future<void> _secondSelectDate(BuildContext context) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: secondSelectedDate,
      firstDate: DateTime(2018),
      lastDate: DateTime.now(),
    );
    picked = picked!.toLocal();

    if (picked != secondSelectedDate)
      setState(() {
        secondSelectedDate = picked!;
      });
  }

  String getSectionName() {
    var answer = apiController.sectionsListAnswer!.data!
        .firstWhere((e) => e.sectionNumber == apiController.sectionNumber);
    String? name = answer.sectionName;
    return name.toString();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      navBarFunction: () {
        table.clear();
        table.add(
          TableRow(
            decoration: BoxDecoration(
                color: Color(0xFF84CC16),
                borderRadius: BorderRadius.circular(5)),
            children: [
              TableRowItem(
                text: 'Дата',
              ),
              TableRowItem(
                text: 'Время',
              ),
              TableRowItem(
                text: 'Зона',
              ),
              TableRowItem(
                text: 'Статус нарушения',
              ),
            ],
          ),
        );
      },
      isContactScreen: false,
      title: 'История тревог',
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white.withOpacity(0.64),
                ),
                height: 90.w,
                width: 335.w,
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 19.w, vertical: 16.w),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            getSectionName(),
                            style: TextStyle(
                              fontSize: 20.sp,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10.w),
                      Row(
                        children: [
                          Text(
                            'Основной (все)',
                            style: TextStyle(fontSize: 17.sp),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 13.w),
                    child: Text(
                      'Поиск по дате',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () => _firstSelectDate(context),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white.withOpacity(0.64),
                          ),
                          child: SizedBox(
                            height: 40.w,
                            width: 130.w,
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'от',
                                    style: TextStyle(
                                        color: Color(0xFF105A89),
                                        fontSize: 14.sp),
                                  ),
                                  Text(
                                    "${firstSelectedDate.toLocal()}"
                                        .split(' ')[0],
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14.sp),
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down_rounded,
                                    color: Colors.black,
                                    size: 20.w,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => _secondSelectDate(context),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white.withOpacity(0.64),
                          ),
                          child: SizedBox(
                            height: 40.w,
                            width: 130.w,
                            child: Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'до',
                                    style: TextStyle(
                                        color: Color(0xFF105A89),
                                        fontSize: 14.sp),
                                  ),
                                  Text(
                                    "${secondSelectedDate.toLocal()}"
                                        .split(' ')[0],
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14.sp),
                                  ),
                                  Icon(
                                    Icons.keyboard_arrow_down_rounded,
                                    color: Colors.black,
                                    size: 20.w,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () => apiController
                            .alertListModel(
                          DateFormat.yMd().format(firstSelectedDate),
                          DateFormat.yMd().format(secondSelectedDate),
                        )
                            .then((value) {
                          table.clear();
                          table.add(
                            TableRow(
                              decoration: BoxDecoration(
                                  color: Color(0xFF84CC16),
                                  borderRadius: BorderRadius.circular(5)),
                              children: [
                                TableRowItem(
                                  text: 'Дата',
                                ),
                                TableRowItem(
                                  text: 'Время',
                                ),
                                TableRowItem(
                                  text: 'Зона',
                                ),
                                TableRowItem(
                                  text: 'Статус нарушения',
                                ),
                              ],
                            ),
                          );
                          if (value.status == Status.success) {
                            for (var i = 0;
                                i < apiController.alertListAnswer!.data!.length;
                                i++) {
                              table.add(tableRow(
                                  AlertModel(
                                    date: apiController
                                        .alertListAnswer!.data![i].threatDate,
                                    time: apiController
                                        .alertListAnswer!.data![i].threatTime,
                                    zone: apiController
                                        .alertListAnswer!.data![i].threatZone,
                                    status: apiController
                                        .alertListAnswer!.data![i].threatName,
                                  ),
                                  null));
                            }
                            setState(() {});
                          }
                        }),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Colors.white.withOpacity(0.64),
                          ),
                          child: SizedBox(
                            height: 40.w,
                            width: 52.w,
                            child: Icon(
                              Icons.search,
                              size: 30.w,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 13.w,
                  ),
                  DecoratedBox(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.white.withOpacity(0.64),
                    ),
                    child: SizedBox(
                      width: 335.w,
                      child: Table(
                        border: TableBorder(
                            top: BorderSide.none,
                            verticalInside:
                                BorderSide(width: 1, color: Colors.white)),
                        children: table,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

TableRow tableRow(AlertModel? alert, Color? color) {
  return TableRow(
      decoration: color != null
          ? BoxDecoration(color: color, borderRadius: BorderRadius.circular(10))
          : null,
      children: [
        TableRowItem(text: alert!.date),
        TableRowItem(text: alert.time),
        TableRowItem(text: alert.zone),
        TableRowItem(text: alert.status),
      ]);
}

class TableRowItem extends StatelessWidget {
  final String? text;
  final tableText = TextStyle(fontSize: 11.sp, color: Colors.black);
  TableRowItem({Key? key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 9.w, vertical: 12.w),
      child: Text(
        text!,
        style: tableText,
        textAlign: TextAlign.center,
      ),
    );
  }
}
