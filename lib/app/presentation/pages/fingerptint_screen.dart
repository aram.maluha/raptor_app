import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:local_auth/local_auth.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';

class BioAuthScreen extends StatefulWidget {
  const BioAuthScreen({Key? key}) : super(key: key);

  @override
  _BioAuthScreenState createState() => _BioAuthScreenState();
}

class _BioAuthScreenState extends State<BioAuthScreen> {
  LocalAuthentication auth = LocalAuthentication();
  bool? _canCheckBiometric;
  List<BiometricType>? _availableBiometric;
  String authorized = "Not authorized";

  Future<bool> authenticateIsAvailable() async {
    final isAvailable = await auth.canCheckBiometrics;
    final isDeviceSupported = await auth.isDeviceSupported();
    return isAvailable && isDeviceSupported;
  }

  Future<void> checkBiometric() async {
    bool? canCheckBiometric;
    try {
      canCheckBiometric = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    print(canCheckBiometric);
    setState(() {
      _canCheckBiometric = canCheckBiometric;
    });
  }

  Future<void> getAvailableBiometrics() async {
    List<BiometricType>? availableBiometric;
    try {
      availableBiometric =
          (await auth.canCheckBiometrics) as List<BiometricType>?;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    print("Available biometric: $_availableBiometric");

    setState(() {
      _availableBiometric = availableBiometric;
    });
  }

  Future<void>? _authenticate() async {
    bool authenticated = false;
    try {
      authenticated = await auth.authenticate(
          localizedReason: "Scan your finger print to authenticate",
          useErrorDialogs: true,
          biometricOnly: true,
          stickyAuth: true);
    } on PlatformException catch (e) {
      print(e);
    }
    // return authenticated;
    if (!mounted) return;

    setState(() {
      authorized =
          authenticated ? "Authorized success" : "Failed to authenticate";
    });
  }

  @override
  void initState() {
    super.initState();
    checkBiometric();
    getAvailableBiometrics();
    print("Available biometric: $_availableBiometric");
  }

  @override
  Widget build(BuildContext context) {
    return
        CircleAvatar(
          radius: 35,
          backgroundColor:Colors.transparent,
          // Color(0xFF105A89).withOpacity(0.64),
          child: IconButton(
          onPressed: _authenticate,
          icon: Icon(Icons.fingerprint_rounded),
           color: Colors.white,
           iconSize: 45.w,
    ),
        );
  }
}
// TextButton(
// onPressed: () {
// Get.to(() => BioAuthScreen());
// },
// child: Text(
// 'FINGERPRINT',
// style: TextStyle(
// fontSize: 20.sp, color: mainColor),
// ),
// ),
// TextButton(
//   onPressed: _authenticate,
//   child: Text("Get Biometric"),
// ),
// Text("Can check biometric: $_canCheckBiometric"),
// Text("Current State: $authorized"),
