import 'dart:async';

import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'package:raptor_app/app/presentation/pages/auth_screen.dart';
import 'package:raptor_app/app/presentation/pages/objects_screen.dart';
import 'package:raptor_app/app/presentation/widgets/drop_down_button.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/services/response_result.dart';

import 'fingerptint_screen.dart';

class PinCodeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PinCodeScreenState();
}

class _PinCodeScreenState extends State<PinCodeScreen> {
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();
  final LocalAuthentication auth = LocalAuthentication();
  bool? _canCheckBiometrics;
  List<BiometricType>? _availableBiometrics;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;
  bool isAuthenticated = false;
  String? storedPasscode = '1111';
  bool authenticated = false;
  @override
  void initState() {
    _authenticate();
    super.initState();
  }

  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  Future<void> _checkBiometrics() async {
    late bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      canCheckBiometrics = false;
      print(e);
    }
    if (!mounted) {
      Get.to(() => AuthScreen());
      return;
    }

    setState(() {
      _canCheckBiometrics = canCheckBiometrics;
    });
  }

  Future<void> _getAvailableBiometrics() async {
    late List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
    } on PlatformException catch (e) {
      availableBiometrics = <BiometricType>[];
      print(e);
    }
    if (!mounted) {
      Get.to(() => AuthScreen());
      return;
    }

    setState(() {
      _availableBiometrics = availableBiometrics;
    });
  }

  Future<void> _authenticate() async {
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });
      authenticated = await auth.authenticate(
        localizedReason: 'Коснитесь сканера для аутентификации',
        useErrorDialogs: true,
        stickyAuth: true,
        androidAuthStrings: AndroidAuthMessages(
            biometricHint: '',
            biometricNotRecognized: 'Не распознано',
            biometricSuccess: 'Аутентификация прошла успешно',
            signInTitle: 'Требуется аутентификация'),
      );
      setState(() {
        _isAuthenticating = false;
      });
    } on PlatformException catch (e) {
      print(e);
      setState(() {
        _isAuthenticating = false;
        _authorized = "Error - ${e.message}";
      });
      return;
    }
    if (!mounted) {
      Get.to(() => AuthScreen());
      return;
    }

    if (authenticated && apiController.isOrgLength == false) {
      apiController.organizationId =
          apiController.organizationsListAnswer!.data![0].organizationId;
      Get.off(() => ObjectsScreen());
      apiController.update();
    } else {
      Get.back();
      apiController.update();
    }
  }

  void _cancelAuthentication() async {
    await auth.stopAuthentication();
    setState(() => _isAuthenticating = false);
  }

  ApiController apiController = Get.find();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ApiController>(
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: true,
        body: Stack(
          children: [
            Image.asset(
              'assets/images/welcome_screen.png',
              fit: BoxFit.fill,
              width: 375.w,
            ),
            // apiController.isOrgLength = false ? SizedBox() :
            Positioned(
              top: 200.w,
              left: 32.w,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ColoredBox(
                    color: Colors.transparent,
                    child: SizedBox(
                      height: 400.w,
                      child: Stack(
                        children: [
                          DecoratedBox(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white,
                            ),
                            child: SizedBox(
                              height: 178.w,
                              width: 295.w,
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 31.w, top: 60.w, right: 31.w),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            SvgPicture.asset(
                                              'assets/images/icons/paper.svg',
                                              color: mainColor,
                                              height: 14.w,
                                            ),
                                            SizedBox(
                                              width: 6.w,
                                            ),
                                            Text(
                                              'Выбор организации',
                                              style: TextStyle(
                                                  color: Color(0xFFBCBCBC),
                                                  fontSize: 12.sp,
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        ),
                                        DropDownButton(),
                                        //  DROPDOWN_SEARCH HERE
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          authenticated == false
                              ? Positioned(
                                  left: 112.w,
                                  right: 112.w,
                                  top: 156.w,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(70),
                                    child: MaterialButton(
                                      color: Color(0xFF105A89),
                                      onPressed: () => _authenticate(),
                                      child: SizedBox(
                                        height: 50.w,
                                        width: 50.w,
                                        child: Icon(
                                          Icons.fingerprint_rounded,
                                          size: 40.w,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ))
                              : Positioned(
                                  left: 62.w,
                                  right: 62.w,
                                  top: 156.w,
                                  child: ArgonButton(
                                    minWidth: 193.w,
                                    height: 44.w,
                                    roundLoadingShape: false,
                                    width: MediaQuery.of(context).size.width *
                                        0.45,
                                    onTap:
                                        (startLoading, stopLoading, btnState) {
                                      if (btnState == ButtonState.Idle) {
                                        startLoading();
                                        apiController
                                            .organizationsChoose(
                                                apiController.organizationId!)
                                            .then(
                                              (value) => {
                                                if (value.status ==
                                                    Status.success)
                                                  {
                                                    Get.offAll(
                                                      () => ObjectsScreen(),
                                                    ),
                                                  }
                                                else
                                                  {
                                                    Get.snackbar(
                                                      'Ошибка',
                                                      value.errorText
                                                          .toString(),
                                                      backgroundColor:
                                                          Colors.red,
                                                      colorText: Colors.white,
                                                    ),
                                                  },
                                                stopLoading(),
                                              },
                                            );
                                      }
                                    },
                                    child: Text(
                                      "Далее",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                      ),
                                    ),
                                    loader: Container(
                                      padding: EdgeInsets.all(10),
                                      child: SpinKitRing(
                                        color: Colors.white,
                                        size: 60.w,
                                        lineWidth: 3.w,
                                      ),
                                    ),
                                    borderRadius: 5.0,
                                    color: mainColor,
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.only(left: 65.w, top: 400.w, right: 65.w),
            //   child: DecoratedBox(
            //     decoration: BoxDecoration(
            //         borderRadius: BorderRadius.circular(8),
            //         color: Colors.white.withOpacity(0.64)),
            //     child: SizedBox(
            //       height: 220.w,
            //       width: 250.w,
            //       child: Column(
            //         mainAxisAlignment: MainAxisAlignment.center,
            //         children: [
            //           DecoratedBox(
            //             decoration: BoxDecoration(
            //                 color: Color(0xFF105A89).withOpacity(0.64),
            //                 borderRadius: BorderRadius.circular(5)),
            //             child: MaterialButton(
            //               onPressed: () => _authenticate(),
            //               child: SizedBox(
            //                   height: 44.w,
            //                   width: 160.w,
            //                   child: Row(
            //                     children: [
            //                       Text(
            //                         'Продолжить авторизацию',
            //                         style: TextStyle(color: Colors.white),
            //                       ),
            //                     ],
            //                   )),
            //             ),
            //           ),
            //           SizedBox(height: 20.w),
            //           DecoratedBox(
            //             decoration: BoxDecoration(
            //                 color: Color(0xFF105A89).withOpacity(0.64),
            //                 borderRadius: BorderRadius.circular(5)),
            //             child: MaterialButton(
            //               onPressed: () {
            //                 Get.to(() => AuthScreen());
            //                 apiController.logOut('');
            //               },
            //               child: Text(
            //                 'Отменить авторизацию',
            //                 style: TextStyle(color: Colors.white),
            //               ),
            //             ),
            //           ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            // Positioned(
            //   bottom: 200.w,
            //   left: 95.w,
            //   child: MaterialButton(
            //     height: 30.w,
            //     minWidth: 90.w,
            //     color: Colors.blue,
            //     child: Text('pincode'),
            //     onPressed: () => _showLockScreen(
            //       context,
            //       opaque: false,
            //       cancelButton: Text(
            //         'Отменить',
            //         style: TextStyle(
            //           fontSize: 16,
            //           color: Colors.white,
            //         ),
            //         semanticsLabel: 'Отменить',
            //       ),
            //     ),
            //   ),
            // )
          ],
        ),
      ),
    );
  }

  _lockScreenButton(BuildContext context) => MaterialButton(
        padding: EdgeInsets.only(left: 50, right: 50),
        color: Theme.of(context).primaryColor,
        child: Text(
          'Lock Screen',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17),
        ),
        onPressed: () {
          _showLockScreen(
            context,
            opaque: false,
            cancelButton: Text(
              'Отменить',
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
              semanticsLabel: 'Отменить',
            ),
          );
        },
      );

  _showLockScreen(BuildContext context,
      {bool? opaque,
      CircleUIConfig? circleUIConfig,
      KeyboardUIConfig? keyboardUIConfig,
      Widget? cancelButton,
      // Widget? fingerprint,
      List<String>? digits}) {
    Navigator.push(
        context,
        PageRouteBuilder(
          opaque: opaque!,
          pageBuilder: (context, animation, secondaryAnimation) =>
              PasscodeScreen(
            title: Text(
              'Введите ПИН-Код',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 22.sp),
            ),
            circleUIConfig: circleUIConfig,
            // CircleUIConfig(
            //   circleSize: 20,
            //   fillColor: mainColor,
            //   borderColor: Color(0xFF105A89),
            // ),
            keyboardUIConfig: KeyboardUIConfig(
              digitFillColor: Colors.transparent,
              // digitFillColor: Color(0xFF105A89).withOpacity(0.64),
              keyboardSize: Size.fromRadius(120),
            ),
            passwordEnteredCallback: _passcodeEntered,
            cancelButton: cancelButton!,
            deleteButton: Text(
              'Удалить',
              style: TextStyle(fontSize: 16.sp, color: Colors.white),
              semanticsLabel: 'Удалить',
            ),
            shouldTriggerVerification: _verificationNotifier.stream,
            backgroundColor: Colors.black.withOpacity(0.8),
            cancelCallback: _passcodeCancelled,
            digits: digits,
            passwordDigits: 4,
            bottomWidget: Padding(
              padding: EdgeInsets.only(top: 90.w),
              child: BioAuthScreen(),
            ),
          ),
        ));
  }

  _passcodeEntered(String enteredPasscode) {
    bool isValid = storedPasscode == enteredPasscode;
    _verificationNotifier.add(isValid);
    if (isValid) {
      setState(() {
        this.isAuthenticated = isValid;
      });
    }
  }

  _passcodeCancelled() {
    Navigator.maybePop(context);
  }

  _passcodeRestoreButton() => Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          margin: EdgeInsets.only(bottom: 10.w, top: 90.w),
          child: MaterialButton(
            child: Text(
              "Сбросить пин-код",
              textAlign: TextAlign.center,
              style: const TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                  fontWeight: FontWeight.w300),
            ),
            splashColor: Colors.white.withOpacity(0.4),
            highlightColor: Colors.white.withOpacity(0.2),
            onPressed: _resetApplicationPassword,
          ),
        ),
      );

  _resetApplicationPassword() {
    Navigator.maybePop(context).then((result) {
      if (!result) {
        return;
      }
      _restoreDialog(() {
        Navigator.maybePop(context);
      });
    });
  }

  _restoreDialog(VoidCallback onAccepted) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.teal[50],
          title: Text(
            "Сбросить",
            style: TextStyle(color: Colors.black87),
          ),
          content: Text(
            "Passcode reset is a non-secure operation!\nAre you sure want to reset?",
            style: TextStyle(color: Colors.black87),
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            TextButton(
              child: Text(
                "Отменить",
                style: TextStyle(fontSize: 18.sp),
              ),
              onPressed: () {
                Navigator.maybePop(context);
              },
            ),
            TextButton(
              child: Text(
                "I proceed",
                style: TextStyle(fontSize: 18.sp),
              ),
              onPressed: onAccepted,
            ),
          ],
        );
      },
    );
  }
}
