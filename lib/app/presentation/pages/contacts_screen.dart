import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/widgets/contacts_info.dart';
import 'package:raptor_app/app/presentation/widgets/custom_scaffold.dart';
import 'package:raptor_app/controllers/api_controller.dart';

class ContactsScreen extends StatefulWidget {
  final String? orgNumber, orgEmail, orgWeb;

  ContactsScreen({Key? key, this.orgNumber, this.orgEmail, this.orgWeb})
      : super(key: key);

  @override
  _ContactsScreenState createState() => _ContactsScreenState();
}

class _ContactsScreenState extends State<ContactsScreen> {
  ApiController apiController = Get.find();
  @override
  void initState() {
    apiController.orgContactsModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CustomScaffold(
          title: 'Контакты',
          body: GetBuilder<ApiController>(
            builder: (_) => apiController.loading &&
                    apiController.orgContactsAnswer?.data == null
                ? Center(
                    child: CircleAvatar(
                        radius: 35.w,
                        backgroundColor: Colors.white.withOpacity(0.64),
                        child: Container(
                          padding: EdgeInsets.all(10.w),
                          height: 75.w,
                          width: 75.w,
                          child: CircularProgressIndicator(
                            color: Colors.blueAccent,
                          ),
                          decoration: BoxDecoration(),
                        )),
                  )
                : SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                            left: 19.w,
                            top: 55.w,
                            right: 19.w,
                          ),
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              // boxShadow: [
                              //   BoxShadow(
                              //       spreadRadius: 1,
                              //       blurRadius: 4,
                              //       color: Colors.white.withOpacity(0.20),
                              //       offset: Offset(2, 1))
                              // ],
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.white.withOpacity(0.54),
                            ),
                            child: SizedBox(
                              width: 335.w,
                              child: ContactsInfo(
                                orgImage: Image.network(
                                    'http://raptor.sport-market.kz/${apiController.orgContactsAnswer!.data?.image}'),
                                orgName:
                                    apiController.orgContactsAnswer!.data?.name,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
          ),
        ),
      ],
    );
  }
}
