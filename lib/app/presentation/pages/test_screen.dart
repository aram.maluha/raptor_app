import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:raptor_app/app/presentation/pages/objects_screen.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:get/get.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/services/response_result.dart';

class TestScreen extends StatefulWidget {
  const TestScreen({Key? key}) : super(key: key);

  @override
  _TestScreenState createState() => _TestScreenState();
}

class _TestScreenState extends State<TestScreen> {
  List<String> items = [
    "Item 1",
    "Item 2",
    "Item 3",
    "Item 4",
    "Item 5",
    "Item 6",
    "Item 7",
    "Item 8"
  ];
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    print(items);
    if (mounted)
      setState(() {
        items.add((items.length + 1).toString());
      });
    _refreshController.loadComplete();
  }

  ApiController apiController = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Data time field"),
      ),
      body: Scrollbar(
        child: SmartRefresher(
          enablePullDown: true,
          header: TwoLevelHeader(),
          footer: CustomFooter(
            builder: (BuildContext context, LoadStatus? status) {
              Widget body;
              if (status == LoadStatus.idle) {
                body = Text("pull up load");
              } else if (status == LoadStatus.loading) {
                body = CupertinoActivityIndicator();
              } else if (status == LoadStatus.failed) {
                body = Text("Load Failed!Click retry!");
              } else if (status == LoadStatus.canLoading) {
                body = Text("release to load more");
              } else {
                body = Text("No more Data");
              }
              return Container(
                height: 100.0,
                child: Center(child: body),
              );
            },
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: ArgonButton(
                  height: 44.w,
                  minWidth: 264,
                  roundLoadingShape: false,
                  width: 193.w,
                  onTap: (startLoading, stopLoading, btnState) async {
                    if (btnState == ButtonState.Idle) {
                      startLoading();
                      await {print('${apiController.organizationId}')};
                      stopLoading();
                      print('${apiController.organizationId}');
                      // Get.to(() => ObjectsScreen());
                    }
                  },
                  child: Text(
                    "Далее",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                    ),
                  ),
                  loader: ArgonButton(
                    minWidth: 193.w,
                    height: 44.w,
                    roundLoadingShape: false,
                    width: MediaQuery.of(context).size.width * 0.45,
                    onTap: (startLoading, stopLoading, btnState) {
                      if (btnState == ButtonState.Idle) {
                        startLoading();
                        apiController
                            .organizationsChoose(apiController.organizationId!)
                            .then(
                              (value) => {
                                if (value.status == Status.success)
                                  {
                                    Get.offAll(
                                      () => ObjectsScreen(),
                                    ),
                                  }
                                else
                                  {
                                    Get.snackbar(
                                      'Ошибка',
                                      value.errorText.toString(),
                                      backgroundColor: Colors.red,
                                      colorText: Colors.white,
                                    ),
                                  },
                                stopLoading(),
                              },
                            );
                      }
                    },
                    child: Text(
                      "Далее",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15,
                      ),
                    ),
                    loader: Container(
                      padding: EdgeInsets.all(10),
                      child: SpinKitRing(
                        color: Colors.white,
                        size: 60.w,
                        lineWidth: 3.w,
                      ),
                    ),
                    borderRadius: 5.0,
                    color: mainColor,
                  ),
                  borderRadius: 5.0,
                  color: mainColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
