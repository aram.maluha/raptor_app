import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/pages/auth_screen.dart';
import 'package:raptor_app/app/presentation/pages/objects_screen.dart';
import 'package:raptor_app/app/presentation/pages/pin_code_screen.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/services/response_result.dart';
import 'package:raptor_app/services/shared_preferences.dart';

class EntryApp extends StatefulWidget {
  @override
  _EntryAppState createState() => _EntryAppState();
}

class _EntryAppState extends State<EntryApp> {
  ApiController apiController = Get.find();
  bool isUserSignedIn = false.obs();
  String userToken = '';
  String userNumber = '';
  String countryCode = '';
  int orgId = 0;
  int status = 0;

  @override
  void initState() {
    _checkLoginStatus();
    apiController.userLocation();
    apiController.organizationsList();
    super.initState();
  }

  Future<bool> _checkLoginStatus() async {
    userToken = PreferencesService.getToken();
    userNumber = PreferencesService.getNumber();
    orgId = PreferencesService.getOrgId();
    countryCode = PreferencesService.getPrefix();
    apiController.number = userNumber;
    apiController.token = userToken;
    apiController.organizationId = orgId;
    int inStatus = 0;
    await apiController.organizationsList().then((value) {
      if (apiController.organizationsListAnswer!.data!.length > 1) {
        apiController.isOrgLength = true;
      }
    });
    await apiController.objectModel().then((value) {
      inStatus = apiController.objectList!.message == 'Операция прошла успешно!'
          ? 200
          : 0;
      status = inStatus;
      if (this.mounted && inStatus == 200) {
        setState(() {
          isUserSignedIn = true;
        });
      }
    });
    return false;
  }

  @override
  Widget build(BuildContext context) {
    print([status, isUserSignedIn]);
    if (isUserSignedIn = true &&
        apiController.objectList!.message == 'Операция прошла успешно!') {
      return PinCodeScreen();
    } else {
      return AuthScreen();
    }
  }
}
