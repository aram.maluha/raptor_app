import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/pages/security_screen.dart';
import 'package:raptor_app/app/presentation/widgets/custom_scaffold.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:raptor_app/app/presentation/widgets/object_sections_widgets.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/models/section_models/controls_button_switch_model.dart';
import 'package:raptor_app/services/response_result.dart';

class ObjectSections extends StatefulWidget {
  const ObjectSections({Key? key}) : super(key: key);

  @override
  _ObjectSectionsState createState() => _ObjectSectionsState();
}

class _ObjectSectionsState extends State<ObjectSections> {
  ApiController apiController = Get.find();
  TextEditingController sectionNameEditController = TextEditingController();
  TextEditingController controlsNameEditController = TextEditingController();

  @override
  void initState() {
    apiController.getAlertButton();
    apiController.controlsModel();
    apiController.sectionsModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> showExitPopup() async {
      return await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              content: Text(
                'Закрыть приложение?',
                style: TextStyle(
                  fontSize: 18.sp,
                ),
              ),
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 30.w, vertical: 20.w),
              actionsPadding: EdgeInsets.symmetric(horizontal: 20.w),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red),
                      ),
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text('Отмена'),
                    ),
                    ElevatedButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: Text('Закрыть'),
                    ),
                  ],
                ),
              ],
            ),
          ) ??
          false;
    }

    return WillPopScope(
      onWillPop: showExitPopup,
      child: CustomScaffold(
        icon: SizedBox(),
        title: 'Разделы объекта',
        body: Padding(
          padding: EdgeInsets.only(left: 20.w, right: 20.w, bottom: 20.w),
          child: GetBuilder<ApiController>(
            builder: (_) => apiController.loading &&
                apiController.sectionsListAnswer?.data == null
                ? Center(
              child: CircleAvatar(
                  radius: 35.w,
                  backgroundColor: Colors.white.withOpacity(0.64),
                  child: Container(
                    padding: EdgeInsets.all(10.w),
                    height: 75.w,
                    width: 75.w,
                    child: CircularProgressIndicator(
                      color: Colors.blueAccent,
                    ),
                    decoration: BoxDecoration(),
                  )),
            )
                : apiController.sectionsListAnswer?.data != null
                    ? SingleChildScrollView(
                      child: Column(
                          children: [
                            ObjectSectionContainer(
                              objectAddress: apiController.objectList!.data!
                                  .firstWhere((e) =>
                                      e.objectId == apiController.objectId)
                                  .objectAddress,
                              objectName: apiController.objectList!.data!
                                  .firstWhere((e) =>
                                      e.objectId == apiController.objectId)
                                  .objectName,
                            ),
                            apiController.alertButton == null
                                ? SizedBox()
                                : SosButton(() {}),
                            ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: apiController
                                    .sectionsListAnswer!.data!.length,
                                itemBuilder: (context, index) {
                                  return SectionContainer(
                                      state: apiController.sectionsListAnswer!
                                          .data![index].sectionState!,
                                      onPressed: () {
                                        // apiController.loading = false;
                                        // apiController.update();
                                        apiController.isPressed = apiController
                                            .sectionsListAnswer!
                                            .data![index]
                                            .sectionState;
                                        apiController.sectionNumber =
                                            apiController.sectionsListAnswer!
                                                .data![index].sectionNumber;
                                        apiController.sectionId = apiController
                                            .sectionsListAnswer!
                                            .data![index]
                                            .sectionId;
                                        Get.to(() => SecurityScreen(),
                                            arguments: [
                                              apiController.sectionsListAnswer!
                                                  .data![index].sectionName!
                                                  .toString(),
                                              apiController.sectionsListAnswer!
                                                  .data![index].sectionState,
                                            ]);
                                      },
                                      iconEdit: () => Get.defaultDialog(
                                            barrierDismissible: false,
                                            titlePadding: EdgeInsets.all(20.w),
                                            contentPadding: EdgeInsets.only(
                                                right: 20.w, left: 20.w),
                                            title: 'Изменить название раздела',
                                            cancel: TextButton(
                                              onPressed: () {
                                                Get.back();
                                              },
                                              child: Text(
                                                'Отменить',
                                                style: TextStyle(
                                                    color: Colors.red),
                                              ),
                                            ),
                                            content: Column(
                                              children: [
                                                Padding(
                                                  padding: EdgeInsets.only(
                                                      left: 50.w,
                                                      right: 50.w,
                                                      bottom: 20.w),
                                                  child: TextFormField(
                                                    controller:
                                                        sectionNameEditController,
                                                    decoration: InputDecoration(
                                                        contentPadding:
                                                            EdgeInsets.zero),
                                                  ),
                                                ),
                                                Text(
                                                  'Если хотите вернуть исходное название - оставьте поле пустым',
                                                  style: TextStyle(
                                                      color: Colors.black38,
                                                      fontSize: 14.sp),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                            confirm: TextButton(
                                              onPressed: () {
                                                apiController.sectionId =
                                                    apiController
                                                        .sectionsListAnswer!
                                                        .data![index]
                                                        .sectionId;
                                                apiController
                                                    .sectionsNameEdit(
                                                        sectionNameEditController
                                                            .text)
                                                    .then((value) {
                                                  if (value.status ==
                                                      Status.success) {
                                                    apiController
                                                            .sectionsListAnswer!
                                                            .data![index]
                                                            .sectionName =
                                                        sectionNameEditController
                                                            .text;
                                                    setState(() {});
                                                    print(value.data);
                                                  }
                                                });
                                                Get.back();
                                              },
                                              style: ButtonStyle(
                                                  // splashFactory: InkSplash.splashFactory
                                                  ),
                                              child: Text('Подтвердить'),
                                            ),
                                          ),
                                      title: apiController.sectionsListAnswer!
                                                  .data![index].sectionLabel ==
                                              null
                                          ? apiController.sectionsListAnswer!
                                              .data![index].sectionName
                                          : apiController.sectionsListAnswer!
                                              .data![index].sectionLabel!);
                                }),
                            SizedBox(height: 18.w),
                            apiController.controlsListAnswer?.data == null
                                ? SizedBox()
                                : DecoratedBox(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(5),
                                      color: Colors.white.withOpacity(0.54),
                                    ),
                                    child: SizedBox(
                                      width: 335.w,
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: 10.w),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left: 13.w, top: 10.w),
                                              child: Text(
                                                'Управление',
                                                style: TextStyle(
                                                  fontSize: 20.sp,
                                                ),
                                              ),
                                            ),
                                            ListView.builder(
                                                shrinkWrap: true,
                                                physics: ScrollPhysics(),
                                                itemCount: apiController
                                                    .controlsListAnswer
                                                    ?.data
                                                    ?.length,
                                                itemBuilder: (context, index) {
                                                  return Column(
                                                    children: [
                                                      ManageSwitcher(
                                                        function: () {
                                                          print([
                                                            "control_id:",
                                                            apiController
                                                                .controlsListAnswer!
                                                                .data![index]
                                                                .control_id
                                                          ]);
                                                          print([
                                                            'control_state',
                                                            apiController
                                                                .controlsListAnswer!
                                                                .data![index]
                                                                .control_state
                                                          ]);
                                                          apiController
                                                              .sectionSwitchModel(
                                                            SectionSwitchRequest(
                                                                control_id: apiController
                                                                    .controlsListAnswer!
                                                                    .data![
                                                                        index]
                                                                    .control_id,
                                                                control_state: apiController
                                                                            .controlsListAnswer!
                                                                            .data![index]
                                                                            .control_state ==
                                                                        0
                                                                    ? 1
                                                                    : 0),
                                                          )
                                                              .then((value) {
                                                            apiController
                                                                .controlsModel()
                                                                .then(
                                                                    (value) => {
                                                                          print([
                                                                            value.statusCode,
                                                                            value.status
                                                                          ]),
                                                                          if (value.status ==
                                                                              Status.success)
                                                                            {}
                                                                        });
                                                          });
                                                        },
                                                        light: apiController
                                                                    .controlsListAnswer!
                                                                    .data![
                                                                        index]
                                                                    .control_state ==
                                                                1
                                                            ? true
                                                            : false,
                                                        title: apiController
                                                                    .controlsListAnswer!
                                                                    .data![
                                                                        index]
                                                                    .control_label ==
                                                                null
                                                            ? apiController
                                                                .controlsListAnswer!
                                                                .data![index]
                                                                .control_name
                                                            : apiController
                                                                .controlsListAnswer!
                                                                .data![index]
                                                                .control_label,
                                                        iconEdit: () =>
                                                            Get.defaultDialog(
                                                          barrierDismissible:
                                                              false,
                                                          titlePadding:
                                                              EdgeInsets.all(
                                                                  20.w),
                                                          contentPadding:
                                                              EdgeInsets.only(
                                                                  right: 20.w,
                                                                  left: 20.w),
                                                          content: Column(
                                                            children: [
                                                              Padding(
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        left: 50
                                                                            .w,
                                                                        right: 50
                                                                            .w,
                                                                        bottom:
                                                                            20.w),
                                                                child:
                                                                    TextFormField(
                                                                  controller:
                                                                      controlsNameEditController,
                                                                  decoration: InputDecoration(
                                                                      contentPadding:
                                                                          EdgeInsets
                                                                              .zero),
                                                                ),
                                                              ),
                                                              Text(
                                                                'Если хотите вернуть исходное название - оставьте поле пустым',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black38,
                                                                    fontSize:
                                                                        14.sp),
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                            ],
                                                          ),
                                                          title:
                                                              'Изменить название секции управления',
                                                          cancel: TextButton(
                                                            onPressed: () {
                                                              Get.back();
                                                            },
                                                            child: Text(
                                                              'Отменить',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .red),
                                                            ),
                                                          ),
                                                          confirm: TextButton(
                                                            onPressed: () {
                                                              apiController
                                                                      .controlId =
                                                                  apiController
                                                                      .controlsListAnswer!
                                                                      .data![
                                                                          index]
                                                                      .control_id;
                                                              apiController
                                                                  .controlsNameEdit(
                                                                      controlsNameEditController
                                                                          .text)
                                                                  .then(
                                                                      (value) {
                                                                if (value
                                                                        .status ==
                                                                    Status
                                                                        .success) {
                                                                  apiController
                                                                          .controlsListAnswer!
                                                                          .data![
                                                                              index]
                                                                          .control_name =
                                                                      controlsNameEditController
                                                                          .text;
                                                                  setState(
                                                                      () {});
                                                                  print(value
                                                                      .data);
                                                                }
                                                              });
                                                              Get.back();
                                                            },
                                                            child: Text(
                                                                'Подтвердить'),
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        height: 7.w,
                                                      ),
                                                      DecoratedBox(
                                                        decoration: BoxDecoration(
                                                            border: Border.all(
                                                                color: Colors
                                                                    .white)),
                                                        child: SizedBox(
                                                          width: 335.w,
                                                          height: 1.w,
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                                }),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                    )
                    : SizedBox(),
          ),
        ),
      ),
    );
  }
}
