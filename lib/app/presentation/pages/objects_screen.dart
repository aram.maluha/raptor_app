import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:raptor_app/app/presentation/pages/auth_screen.dart';
import 'package:raptor_app/app/presentation/pages/object_sections.dart';
import 'package:raptor_app/app/presentation/widgets/custom_scaffold.dart';
import 'package:raptor_app/app/presentation/widgets/objects_components.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/services/response_result.dart';

class ObjectsScreen extends StatefulWidget {
  const ObjectsScreen({Key? key}) : super(key: key);

  @override
  _ObjectsScreenState createState() => _ObjectsScreenState();
}

class _ObjectsScreenState extends State<ObjectsScreen> {
  ApiController apiController = Get.find();
  TextEditingController objectNameEditController = TextEditingController();

  @override
  void initState() {
    apiController.objectModel();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> showExitPopup() async {
      return await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              content: Text(
                'Закрыть приложение?',
                style: TextStyle(
                  fontSize: 18.sp,
                ),
              ),
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 30.w, vertical: 20.w),
              actionsPadding: EdgeInsets.symmetric(horizontal: 20.w),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red),
                      ),
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text('Отмена'),
                    ),
                    ElevatedButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: Text('Закрыть'),
                    ),
                  ],
                ),
              ],
            ),
          ) ??
          false;
    }

    return WillPopScope(
      onWillPop: showExitPopup,
      child: CustomScaffold(
        icon: IconButton(
          onPressed: () {
            setState(() {
              Get.offAll(() => AuthScreen());
              apiController.logOut('5');
            });
          },
          iconSize: 25,
          color: Colors.white,
          splashRadius: 20,
          icon: Icon(Icons.logout),
        ),
        title: 'Мои объекты',
        body: GetBuilder<ApiController>(
          builder: (_) => apiController.loading &&
                  apiController.objectList?.data == null
              ? Center(
                  child: CircleAvatar(
                      radius: 35.w,
                      backgroundColor: Colors.white.withOpacity(0.64),
                      child: Container(
                        padding: EdgeInsets.all(10.w),
                        height: 75.w,
                        width: 75.w,
                        child: CircularProgressIndicator(
                          color: Colors.blueAccent,
                        ),
                        decoration: BoxDecoration(),
                      )),
                )
              : apiController.objectList?.data != null
                  ? Column(
                      children: [
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 16.w),
                            child: ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: apiController.objectList!.data?.length,
                              itemBuilder: (context, index) {
                                return ObjectContainer(
                                  onPressed: () {
                                    apiController.objectId = apiController
                                        .objectList!.data![index].objectId;
                                    Get.offAll(
                                      () => ObjectSections(),
                                      arguments: [
                                        apiController.objectList?.data?[index]
                                            .objectAddress,
                                        apiController.objectList?.data?[index]
                                            .objectName,
                                      ],
                                    );
                                  },
                                  iconEdit: () => Get.defaultDialog(
                                    titlePadding: EdgeInsets.all(20.w),
                                    title: 'Изменить название объекта',
                                    cancel: TextButton(
                                      onPressed: () {
                                        Get.back();
                                      },
                                      child: Text(
                                        'Отменить',
                                        style: TextStyle(color: Colors.red),
                                      ),
                                    ),
                                    content: Column(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(
                                              left: 50.w,
                                              right: 50.w,
                                              bottom: 20.w),
                                          child: TextFormField(
                                            controller:
                                                objectNameEditController,
                                            decoration: InputDecoration(
                                                contentPadding:
                                                    EdgeInsets.zero),
                                          ),
                                        ),
                                        Text(
                                          'Если хотите вернуть исходное название - оставьте поле пустым',
                                          style: TextStyle(
                                              color: Colors.black38,
                                              fontSize: 14.sp),
                                          textAlign: TextAlign.center,
                                        ),
                                      ],
                                    ),
                                    confirm: TextButton(
                                      onPressed: () {
                                        apiController.objectId = apiController
                                            .objectList!.data![index].objectId;
                                        apiController
                                            .objectNameEdit(
                                                objectNameEditController.text)
                                            .then((value) {
                                          if (value.status == Status.success) {
                                            apiController.objectList!
                                                    .data![index].objectName =
                                                objectNameEditController.text;
                                            setState(() {});
                                            print(value.data);
                                          }
                                        });
                                        Get.back();
                                      },
                                      style: ButtonStyle(
                                          // splashFactory: InkSplash.splashFactory
                                          ),
                                      child: Text('Подтвердить'),
                                    ),
                                  ),
                                  objectAddress: apiController
                                      .objectList?.data?[index].objectAddress,
                                  objectName: apiController.objectList
                                              ?.data?[index].objectLabel ==
                                          null
                                      ? apiController
                                          .objectList?.data![index].objectName
                                      : apiController
                                          .objectList?.data![index].objectLabel,
                                );
                              },
                            ),
                          ),
                        ),
                        // SizedBox(height: 20.w),
                      ],
                    )
                  : SizedBox(),
        ),
      ),
    );
  }
}

//CircleAvatar(
//                     radius: 75.w,
//                     backgroundColor: Colors.white.withOpacity(0.64),
//                     child: Container(
//                       padding: EdgeInsets.all(10.w),
//                       height: 130.w,
//                       width: 130.w,
//                       child: CircularProgressIndicator(
//                         color: Colors.blueAccent,
//                       ),
//                       decoration: BoxDecoration(),
//                     )),
