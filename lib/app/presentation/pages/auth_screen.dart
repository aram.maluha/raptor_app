import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/widgets/auth_forms/auth_first_form.dart';
import 'package:raptor_app/app/presentation/widgets/auth_forms/reg_first_form.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
import 'package:raptor_app/common/raptor_style.dart';
import 'package:raptor_app/controllers/api_controller.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  @override
  Widget build(BuildContext context) {
    Future<bool> showExitPopup() async {
      return await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              content: Text(
                'Закрыть приложение?',
                style: TextStyle(
                  fontSize: 18.sp,
                ),
              ),
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 30.w, vertical: 20.w),
              actionsPadding: EdgeInsets.symmetric(horizontal: 20.w),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red),
                      ),
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text('Отмена'),
                    ),
                    ElevatedButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: Text('Закрыть'),
                    ),
                  ],
                ),
              ],
            ),
          ) ??
          false;
    }

    return WillPopScope(
      onWillPop: showExitPopup,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        // extendBody: true
        body: DecoratedBox(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/auth_back.png'),
                fit: BoxFit.fitWidth),
          ),
          child: SingleChildScrollView(
            physics: ScrollPhysics(),
            child: SizedBox(
              // width: 375.w,
              height: 812.w,
              child: Padding(
                padding: EdgeInsets.only(top: 125.w),
                child: AuthScreenForms(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class AuthScreenForms extends StatefulWidget {
  const AuthScreenForms({
    Key? key,
  }) : super(key: key);

  @override
  _AuthScreenFormsState createState() => _AuthScreenFormsState();
}

class _AuthScreenFormsState extends State<AuthScreenForms> {
  ApiController apiController = Get.find();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: GetBuilder<ApiController>(
        builder: (_) => Stack(
          children: [
            Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 40.w, right: 40.w, top: 40.w),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      AuthTopText(
                        onTap: () {
                          setState(() {
                            isAuthForm = true;
                            controller.animateToPage(0,
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeInSine);
                          });
                        },
                        title: 'Вход',
                        selected: isAuthForm,
                      ),
                      AuthTopText(
                        onTap: () {
                          setState(() {
                            isAuthForm = false;
                            controller.animateToPage(1,
                                duration: Duration(milliseconds: 300),
                                curve: Curves.easeInSine);
                          });
                        },
                        title: 'Регистрация',
                        selected: !isAuthForm,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 295.w,
                  height: 450.w,
                  child: PageView(
                    clipBehavior: Clip.antiAlias,
                    controller: controller,
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: [
                      AuthFirstForm(),
                      RegFirstForm(),
                    ],
                  ),
                ),
              ],
            ),
            apiController.loading == true
                ? Positioned(
                    left: 170.w,
                    top: 260.w,
                    child: Center(
                      child: Container(
                        padding: EdgeInsets.all(10.w),
                        child: CircularProgressIndicator(
                          color: Colors.blueAccent,
                        ),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 0.2,
                                blurRadius: 5,
                                offset: Offset(-0.8, 0.5))
                          ],
                          color: Colors.white.withOpacity(0.64),
                          borderRadius: BorderRadius.circular(40),
                        ),
                      ),
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  bool isAuthForm = true;

  PageController controller = PageController();

  @override
  void initState() {
    super.initState();
  }
}

class AuthTopText extends StatefulWidget {
  final String? title;
  final Function()? onTap;
  final bool? selected;
  AuthTopText({this.onTap, this.title, this.selected});
  @override
  _AuthTopTextState createState() => _AuthTopTextState();
}

class _AuthTopTextState extends State<AuthTopText> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: SizedBox(
        height: 50,
        width: 100,
        child: DecoratedBox(
          decoration: widget.selected == true
              ? RaptorStyle.kTopTextDecoration
              : RaptorStyle.kTopTextUnselectedDecoration,
          child: Padding(
            padding: EdgeInsets.only(top: 24),
            child: SizedBox(
              height: 14,
              width: 50,
              child: Text(
                '${widget.title}',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: widget.selected == true ? mainColor : Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
