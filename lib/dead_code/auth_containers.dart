// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:dropdown_search/dropdown_search.dart';
//
// import '../app/presentation/widgets/main_widgets.dart';

// class RegContainer extends StatelessWidget {
// @override
// Widget build(BuildContext context) {
// return ColoredBox(
//   color: Colors.transparent,
//   child: SizedBox(
//     height: 400.w,
//     child: Stack(
//       children: [
//         DecoratedBox(
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(5),
//             color: Colors.white,
//           ),
//           child: SizedBox(
//             height: 178.w,
//             width: 295.w,
//             child: Column(
//               children: [
//                 Padding(
//                   padding:
//                       EdgeInsets.only(left: 32.w, top: 60.w, right: 44.w),
//                   child: Column(
//                     children: [
//                       Row(
//                         children: [
//                           Icon(
//                             Icons.call,
//                             color: mainColor,
//                             size: 15.w,
//                           ),
//                           Text(
//                             ' Телефон',
//                             style: TextStyle(
//                                 color: Color(0xFFBCBCBC),
//                                 fontSize: 12.sp,
//                                 fontWeight: FontWeight.w500),
//                           )
//                         ],
//                       ),
//                       DecoratedBox(
//                         decoration: BoxDecoration(),
//                         child: SizedBox(
//                           child: TextFormField(
//                             textAlign: TextAlign.left,
//                             decoration: InputDecoration(
//                               contentPadding: EdgeInsets.only(left: 5.w),
//                               labelText: '+7',
//                               floatingLabelBehavior:
//                                   FloatingLabelBehavior.never,
//                               labelStyle: TextStyle(
//                                   color: Colors.black, fontSize: 12.sp),
//                               hintText: 'Номер телефона',
//                               hintStyle: TextStyle(
//                                   fontSize: 12.sp,
//                                   color: Colors.black.withOpacity(0.64)),
//                             ),
//                           ),
//                           width: 209.w,
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ),
//         Positioned(
//           left: 51.w,
//           top: 156.w,
//           child: GestureDetector(
//             onTap: () {
//               Navigator.of(context).pushReplacementNamed('objects_screen');
//             },
//             child: DecoratedBox(
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(5),
//                 color: mainColor,
//               ),
//               child: SizedBox(
//                 height: 44.w,
//                 width: 193.w,
//                 child: Center(
//                   child: Text(
//                     'Далее',
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ],
//     ),
//   ),
// );
// }
// }
//old second reg_container
// class RegContainerSecond extends StatelessWidget {
//   const RegContainerSecond({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return ColoredBox(
//       color: Colors.transparent,
//       child: Stack(
//         children: [
//           DecoratedBox(
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(5),
//               color: Colors.white,
//             ),
//             child: SizedBox(
//               width: 295.w,
//               child: Padding(
//                 padding: EdgeInsets.symmetric(horizontal: 25.w, vertical: 70.w),
//                 child: Column(
//                   children: [
//                     Column(
//                       children: [
//                         Row(
//                           children: [
//                             Icon(
//                               Icons.lock,
//                               size: 15.w,
//                               color: mainColor,
//                             ),
//                             Text(
//                               'Пароль',
//                               style: hintText,
//                             )
//                           ],
//                         ),
//                         TextField(),
//                       ],
//                     ),
//                     Column(
//                       children: [
//                         Row(
//                           children: [
//                             Icon(
//                               Icons.lock,
//                               size: 15.w,
//                               color: mainColor,
//                             ),
//                             Text('Повторите пароль')
//                           ],
//                         ),
//                         TextField(),
//                       ],
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           GestureDetector(
//             onTap: () {
//               Navigator.of(context).pushReplacementNamed('objects_screen');
//             },
//             child: DecoratedBox(
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(5),
//                 color: mainColor,
//               ),
//               child: SizedBox(
//                 height: 44.w,
//                 width: 193.w,
//                 child: Center(
//                   child: Text(
//                     'Зарегистрироваться',
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

// class AuthContainer extends StatefulWidget {
//   final VoidCallback? onPressed;
//
//   const AuthContainer({Key? key, this.onPressed}) : super(key: key);
//
//   @override
//   _AuthContainerState createState() => _AuthContainerState();
// }

// class _AuthContainerState extends State<AuthContainer> {
//   @override
//   Widget build(BuildContext context) {
//     return ColoredBox(
//       color: Colors.transparent,
//       child: SizedBox(
//         height: 400.w,
//         child: Stack(
//           children: [
//             DecoratedBox(
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(5),
//                 color: Colors.white,
//               ),
//               child: SizedBox(
//                 width: 295.w,
//                 height: 306.w,
//                 child: Padding(
//                   padding:
//                       EdgeInsets.symmetric(horizontal: 25.w, vertical: 50.w),
//                   child: Column(
//                     children: [
//                       Padding(
//                         padding: EdgeInsets.only(bottom: 8.w),
//                         child: Column(
//                           children: [
//                             Row(
//                               children: [
//                                 SvgPicture.asset(
//                                   'assets/images/icons/call_number.svg',
//                                   height: 14.w,
//                                   color: mainColor,
//                                 ),
//                                 Text(
//                                   '  Телефон',
//                                   style: hintText,
//                                 )
//                               ],
//                             ),
//                             TextField(),
//                           ],
//                         ),
//                       ),
//                       Padding(
//                         padding: EdgeInsets.only(bottom: 8.w),
//                         child: Column(
//                           children: [
//                             Row(
//                               children: [
//                                 SvgPicture.asset(
//                                   'assets/images/icons/lock.svg',
//                                   height: 14.w,
//                                   color: mainColor,
//                                 ),
//                                 Text(
//                                   '  Пароль',
//                                   style: hintText,
//                                 )
//                               ],
//                             ),
//                             TextField(),
//                           ],
//                         ),
//                       ),
//                       Padding(
//                         padding: EdgeInsets.only(bottom: 8.w),
//                         child: Column(
//                           children: [
//                             Row(
//                               children: [
//                                 SvgPicture.asset(
//                                   'assets/images/icons/paper.svg',
//                                   color: mainColor,
//                                   height: 14.w,
//                                 ),
//                                 Text(
//                                   '  Выбор организации',
//                                   style: hintText,
//                                 )
//                               ],
//                             ),
//                             SizedBox(
//                               height: 50.w,
//                               child: DropdownSearch(
//                                 mode: Mode.MENU,
//                                 showSelectedItem: true,
//                                 items: [
//                                   'Организация 1',
//                                   'Организация 2',
//                                   'Организация 3'
//                                 ],
//                                 dropdownSearchDecoration: InputDecoration(
//                                   border: UnderlineInputBorder(),
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                       Row(
//                         children: [
//                           TextButton(
//                             onPressed: () {},
//                             style: TextButton.styleFrom(
//                               padding: EdgeInsets.zero,
//                             ),
//                             child: Text(
//                               'Забыли пароль?',
//                               style: TextStyle(
//                                   color: mainColor,
//                                   fontWeight: FontWeight.w500,
//                                   fontSize: 16.sp),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//             Positioned(
//               top: 284.w,
//               left: 51.w,
//               child: ClipRRect(
//                 borderRadius: BorderRadius.circular(10),
//                 child: MaterialButton(
//                   // splashColor: Colors.white.withOpacity(0.32),
//                   materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
//                   minWidth: 200.w,
//                   height: 72.w,
//                   color: mainColor,
//                   onPressed: () {
//                     Navigator.of(context)
//                         .pushReplacementNamed('objects_screen');
//                   },
//                   child: Text(
//                     'Войти',
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 15.sp,
//                       fontWeight: FontWeight.w500,
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class RegContainerPassword extends StatefulWidget {
//   const RegContainerPassword({Key? key}) : super(key: key);
//
//   @override
//   _RegContainerPasswordState createState() => _RegContainerPasswordState();
// }
//
// class _RegContainerPasswordState extends State<RegContainerPassword> {
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }
