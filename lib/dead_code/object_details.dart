// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:raptor_app/app/presentation/pages/security_screen.dart';
// import 'package:raptor_app/app/presentation/widgets/custom_scaffold.dart';
// import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
//
// class ObjectDetails extends StatefulWidget {
//   ObjectDetails({Key? key}) : super(key: key);
//
//   @override
//   _ObjectDetailsState createState() => _ObjectDetailsState();
// }
//
// class _ObjectDetailsState extends State<ObjectDetails> {
//   @override
//   Widget build(BuildContext context) {
//     return CustomScaffold(
//       title: 'Разделы объекта',
//       body: SingleChildScrollView(
//         child: Padding(
//           padding: EdgeInsets.symmetric(horizontal: 19.w),
//           child: Column(
//             children: [
//
//               SizedBox(
//                 height: 18.w,
//               ),
//
//               SizedBox(
//                 height: 30.w,
//               ),
//               SectionContainer(
//                 title: 'Этаж 1',
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(builder: (context) => SecurityScreen()),
//                   );
//                 },
//                 iconEdit: Icon(
//                   Icons.lock_outline_rounded,
//                   color: Colors.red,
//                 ),
//               ),
//               SectionContainer(
//                 title: 'Этаж 2',
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(builder: (context) => SecurityScreen()),
//                   );
//                 },
//                 iconEdit: Icon(
//                   Icons.lock_open,
//                   color: mainColor,
//                 ),
//               ),
//               SectionContainer(
//                 title: 'Этаж 3',
//                 iconEdit: Icon(
//                   Icons.lock_open,
//                   color: mainColor,
//                 ),
//                 onTap: () {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(builder: (context) => SecurityScreen()),
//                   );
//                 },
//               ),
//               SizedBox(
//                 height: 18.w,
//               ),
//
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
