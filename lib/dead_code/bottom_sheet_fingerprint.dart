// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:raptor_app/app/presentation/pages/fingerptint_screen.dart';
// import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
//
// class BottomSheetFingerprint extends StatefulWidget {
//   @override
//   _BottomSheetFingerprintState createState() =>
//       new _BottomSheetFingerprintState();
// }
//
// class _BottomSheetFingerprintState extends State<BottomSheetFingerprint> {
//   final _scaffoldKey = GlobalKey<ScaffoldState>();
//   VoidCallback? _showPersistantBottomSheetCallBack;
//   @override
//   void initState() {
//     super.initState();
//     _showPersistantBottomSheetCallBack = _showBottomSheet;
//   }
//
//   void _showBottomSheet() {
//     setState(() {
//       _showPersistantBottomSheetCallBack = null;
//     });
//
//     _scaffoldKey.currentState!
//         .showBottomSheet((context) {
//           return DecoratedBox(
//             decoration: BoxDecoration(
//               boxShadow: [
//                 BoxShadow(
//                   blurRadius: 1,
//                   spreadRadius: 1,
//                   color: Colors.black12,
//                   offset: Offset(1, 1),
//                 ),
//               ],
//               color: Colors.white,
//               borderRadius: BorderRadius.vertical(
//                 top: Radius.circular(20),
//                 // left: Radius.circular(20),
//               ),
//             ),
//             child: SizedBox(
//               height: 200.w,
//               width: 375.w,
//               child: Column(
//                 children: [
//                   SizedBox(height: 20.w),
//                   Text(
//                     "Выберите способ входа в приложение",
//                     style: TextStyle(fontSize: 18, color: Colors.black),
//                     textAlign: TextAlign.center,
//                   ),
//                   SizedBox(
//                     height: 20.w,
//                   ),
//                   ClipRRect(
//                     borderRadius: BorderRadius.circular(5),
//                     child: MaterialButton(
//                       minWidth: 200.w,
//                       height: 45.w,
//                       color: mainColor,
//                       onPressed: () {},
//                       child: Text(
//                         'Авторизоваться с номером',
//                         style: TextStyle(color: Colors.white),
//                       ),
//                     ),
//                   ),
//                   SizedBox(
//                     height: 20.w,
//                   ),
//                   ClipRRect(
//                     borderRadius: BorderRadius.circular(5),
//                     child: MaterialButton(
//                       height: 45.w,
//                       minWidth: 200.w,
//                       color: Colors.blueAccent,
//                       onPressed: () {},
//                       child: Text(
//                         'Использовать отпечаток',
//                         style: TextStyle(color: Colors.white),
//                       ),
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           );
//         })
//         .closed
//         .whenComplete(() {
//           if (mounted) {
//             setState(() {
//               _showPersistantBottomSheetCallBack = _showBottomSheet;
//             });
//           }
//         });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//         key: _scaffoldKey,
//         body: Column(
//           children: [
//             Center(
//               child: Container(
//                 child: MaterialButton(
//                   onPressed: _showPersistantBottomSheetCallBack,
//                   child: Text('Show bottom sheet'),
//                 ),
//                 color: mainColor,
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
