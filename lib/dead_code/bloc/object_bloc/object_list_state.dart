// import 'package:equatable/equatable.dart';
// import 'package:meta/meta.dart';
// import 'package:raptor_app/app/domain/entities/object_entity.dart';
//
// abstract class ObjectState extends Equatable{
//   const ObjectState();
//
//   @override
//   List<Object> get props => [];
// }
//
// class ObjectEmpty extends ObjectState{
//   @override
//   List<Object> get props => [];
// }
// class ObjectLoading extends ObjectState{
//   final List<ObjectEntity> oldObjectList;
//
//   ObjectLoading(this.oldObjectList,);
//
//   @override
//   List<Object> get props => [oldObjectList,];
// }
//
// class ObjectLoaded extends ObjectState{
//   final List<ObjectEntity> objectList;
//
//   ObjectLoaded(this.objectList);
//
//   @override
//   List<Object> get props => [objectList];
// }
// class ObjectError extends ObjectState{
//   final String? message;
//
//   ObjectError({@required this.message});
//
//   @override
//   List<Object> get props => [message!];
//
// }
