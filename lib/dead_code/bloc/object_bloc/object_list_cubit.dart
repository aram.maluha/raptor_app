// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:meta/meta.dart';
// import 'package:raptor_app/app/domain/entities/object_entity.dart';
// import 'package:raptor_app/app/domain/usecases/object_fetch.dart';
// import 'package:raptor_app/app/presentation/bloc/object_bloc/object_list_state.dart';
// import 'package:raptor_app/core/error/failure.dart';
//
// class ObjectListCubit extends Cubit<ObjectState> {
//   final ObjectFetch? objectFetch;
//
//   ObjectListCubit({@required this.objectFetch}) : super(ObjectEmpty());
//
//   int page = 1;
//
//   void loadObjects() async {
//     if (state is ObjectLoading) return;
//     final currentState = state;
//
//     var oldObjects = <ObjectEntity>[];
//
//     if (currentState is ObjectLoaded) {
//       oldObjects = currentState.objectList;
//     }
//     emit(ObjectLoading(
//       oldObjects,
//     ));
//
//     final failureOrCharacter = await objectFetch!();
//
//     failureOrCharacter
//         .fold((failure) => ObjectError(message: _mapFailureToMessage(failure)),
//             (characters) {
//       page++;
//       final charactersTemp = (state as ObjectLoading).oldObjectList;
//       charactersTemp.addAll(characters);
//
//       emit(ObjectLoaded(charactersTemp));
//     });
//   }
//
//   _mapFailureToMessage(Failure failure) {
//     switch (failure.runtimeType) {
//       case ServerFailure:
//         return 'Server Failure';
//       case CacheFailure:
//         return 'Cache Failure';
//       default:
//         return 'Unexpected Error';
//     }
//   }
// }
