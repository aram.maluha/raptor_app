// import 'package:get_it/get_it.dart';
// import 'package:internet_connection_checker/internet_connection_checker.dart';
// import 'package:http/http.dart' as http;
// import 'package:raptor_app/app/data/datasources/object_local_datasource.dart';
// import 'package:raptor_app/app/data/datasources/object_remote_datasource.dart';
// import 'package:raptor_app/app/data/repositories/object_repository_implementation.dart';
// import 'package:raptor_app/app/domain/repositories/object_repository.dart';
// import 'package:raptor_app/app/domain/usecases/object_fetch.dart';
// import 'package:raptor_app/app/presentation/bloc/object_bloc/object_list_cubit.dart';
// import 'package:raptor_app/core/platform/network_info.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// final sl = GetIt.instance;
//
// Future<void> init() async{
//   // BloC/ Cubit
//   sl.registerFactory(
//           () => ObjectListCubit(objectFetch: sl()),
//   );
//
//   // UseCases
//   sl.registerLazySingleton(
//           () => ObjectFetch(sl()),
//   );
//   // Repository
//   sl.registerLazySingleton<ObjectRepository>(
//         () => ObjectRepositoryImplementation(
//             networkInfo: sl(),
//             localDataSource: sl(),
//             remoteDataSource: sl(),
//         ),
//   );
//
//   sl.registerLazySingleton<ObjectRemoteDataSource>(
//         () => ObjectRemoteDataSourceImplementation(
//           client: http.Client(),
//         ),
//   );
//
//   sl.registerLazySingleton<ObjectLocalDataSource>(
//         () => ObjectLocalDataSourceImplementation(
//               sharedPreferences: sl(),
//         ),
//   );
//   // Core
//   sl.registerLazySingleton<NetworkInfo>(
//           () => NetworkInfoImplementation(
//                 connectionChecker: sl(),
//           ),
//   );
//
//   // External
//   final sharedPreferences = await SharedPreferences.getInstance();
//   sl.registerLazySingleton(()  => sharedPreferences);
//   sl.registerLazySingleton(() => http.Client());
//   sl.registerLazySingleton(() => InternetConnectionChecker());
//
// }
