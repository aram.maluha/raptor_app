// import 'dart:convert';
//
// import 'package:raptor_app/app/data/models/object_list_model.dart';
// import 'package:raptor_app/core/error/exception.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:meta/meta.dart';
// abstract class ObjectLocalDataSource{
//
//   //Вытаскивает из кэша последних персонажей когда нет подключения к интернету
//   //Кидает [CacheException]
//
//   Future<List<ObjectModel>> getLastObjectsFromCache();
//
//   Future<void> objectsToCache(List<ObjectModel> characters);
// }
//
// class ObjectLocalDataSourceImplementation implements ObjectLocalDataSource{
//   final SharedPreferences? sharedPreferences;
//
//   ObjectLocalDataSourceImplementation({@required this.sharedPreferences});
//
//   @override
//   Future<void> objectsToCache(List<ObjectModel> characters) async{
//     final List<String> charactersJson = characters.map((character) => jsonEncode(character.toJson())).toList();
//
//     sharedPreferences!.setStringList('CACHED_OBJECTS', charactersJson);
//     return Future.value(charactersJson);
//   }
//
//   @override
//   Future<List<ObjectModel>> getLastObjectsFromCache() async{
//     final objectJson = sharedPreferences!.getStringList('CACHED_OBJECTS');
//     if(objectJson!.isNotEmpty){
//       return Future.value(objectJson.map((object) => ObjectModel.fromJson(jsonDecode(object))).toList());
//     }else{
//       throw CacheException();
//     }
//   }
//
// }
