// import 'dart:convert';
//
// import 'package:http/http.dart' as http;
// import 'package:meta/meta.dart';
// import 'package:raptor_app/app/data/models/object_list_model.dart';
// import 'package:raptor_app/core/error/exception.dart';
//
// abstract class ObjectRemoteDataSource {
//   // Вызывает "https://rickandmortyapi.com/api/character/?page=$page"
//   // Throws [ServerException] for all codes
//   Future<List<ObjectModel>?> getAllObjects();
// }
//
// class ObjectRemoteDataSourceImplementation implements ObjectRemoteDataSource {
//   final http.Client? client;
//
//   ObjectRemoteDataSourceImplementation({@required this.client});
//
//   @override
//   Future<List<ObjectModel>?> getAllObjects() async {
//     return _getObjectsFromJson(
//         'http://raptor.sport-market.kz/api/v1/organization/objects');
//   }
//
//   Future<List<ObjectModel>?> _getObjectsFromJson(String url) async {
//     final response = await client!
//         .get(Uri.parse(url), headers: {'Content-Type': 'application/json'});
//     if (response.statusCode == 200) {
//       if (json.isNotEmpty) {
//         return (jsonDecode(response.body)['results'] as List)
//             .map((object) => ObjectModel.fromJson(object))
//             .toList();
//         return json.map((object) => ObjectModel.fromJson(object)).toList();
//       } else {
//         throw ServerException();
//       }
//     }
//   }
//
//   List<Map<String, dynamic>> json = [
//     {
//       'id': 1,
//       'name': 'БЦ МТС',
//       'status': false,
//       'address': 'Шевченко 165Б',
//       'stages': {
//         {'id': 1, 'name': 'Кабинет 511', 'status': false},
//         {'id': 2, 'name': 'Кабинет 1009', 'status': false}
//       },
//       'alertManagement': true,
//       'techManagement': false,
//       'secureManagement': false,
//     },
//     {
//       'id': 2,
//       'name': 'TЦ Меga Almaty',
//       'status': false,
//       'address': 'Розыбаекиева 135',
//       'stages': [
//         {'id': 3, 'name': 'Кабинет 511', 'status': false}
//       ],
//       'alertManagement': true,
//       'techManagement': false,
//       'secureManagement': false,
//     }
//   ];
// }
