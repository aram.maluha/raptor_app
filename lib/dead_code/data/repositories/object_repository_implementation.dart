// import 'package:dartz/dartz.dart';
// import 'package:meta/meta.dart';
// import 'package:raptor_app/app/data/datasources/object_local_datasource.dart';
// import 'package:raptor_app/app/data/datasources/object_remote_datasource.dart';
// import 'package:raptor_app/app/data/models/object_list_model.dart';
// import 'package:raptor_app/app/domain/entities/object_entity.dart';
// import 'package:raptor_app/app/domain/repositories/object_repository.dart';
// import 'package:raptor_app/core/error/exception.dart';
// import 'package:raptor_app/core/error/failure.dart';
// import 'package:raptor_app/core/platform/network_info.dart';
//
// class ObjectRepositoryImplementation implements ObjectRepository {
//   final ObjectRemoteDataSource? remoteDataSource;
//   final ObjectLocalDataSource? localDataSource;
//   final NetworkInfo? networkInfo;
//
//   ObjectRepositoryImplementation(
//       {@required this.remoteDataSource,
//       @required this.localDataSource,
//       @required this.networkInfo});
//
//   @override
//   Future<Either<Failure, List<ObjectEntity>>> getAllObjects() async {
//     return await _getObjectsData(remoteDataSource!.getAllObjects());
//   }
//
//   Future<Either<Failure, List<ObjectModel>>> _getObjectsData(
//       Future<List<ObjectModel>?> getObjects) async {
//     if (await networkInfo!.isConnected) {
//       try {
//         final remoteObjects = await getObjects;
//         localDataSource!.objectsToCache(remoteObjects!);
//         return Right(remoteObjects);
//       } on ServerException {
//         return Left(ServerFailure());
//       }
//     } else {
//       try {
//         final localObjects = await localDataSource!.getLastObjectsFromCache();
//         return Right(localObjects);
//       } on CacheException {
//         return Left(CacheFailure());
//       }
//     }
//   }
// }
