// import 'dart:convert';
//
// import 'package:raptor_app/app/domain/entities/object_entity.dart';
// import 'package:meta/meta.dart';
//
// class ObjectModel extends ObjectEntity {
//   ObjectModel({
//     @required id,
//     @required name,
//     @required status,
//     @required address,
//     @required stages,
//     @required alertManagement,
//     @required secureManagement,
//     @required techManagement,
//   }) : super(
//           id: id,
//           name: name,
//           status: status,
//           address: address,
//           stages: stages,
//           alertManagement: alertManagement,
//           secureManagement: secureManagement,
//           techManagement: techManagement,
//         );
//
//   factory ObjectModel.fromJson(Map<String, dynamic> json) {
//     return ObjectModel(
//       id: json['id'],
//       name: json['name'],
//       status: json['status'],
//       address: json['address'],
//       stages: null,
//       // stages: json['stages'] != null ? StageModel.fromJson(json['stages']) : null,
//       alertManagement: json['alertManagement'],
//       techManagement: json['techManagement'],
//       secureManagement: json['secureManagement'],
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     return {
//       'id': id,
//       'name': name,
//       'status': status,
//       'address': address,
//       'stages': stages,
//       'alertManagement': alertManagement,
//       'techManagement': secureManagement,
//     };
//   }
// }
//
// class StageModel extends StageEntity {
//   StageModel({
//     @required id,
//     @required name,
//     @required status,
//   }) : super(id: id, name: name, status: status);
//
//   factory StageModel.fromJson(Map<String, Object> json) {
//     return StageModel(
//       id: json['id'],
//       name: json['name'],
//       status: json['status'],
//     );
//   }
//
//   Map<String, dynamic> toJson() {
//     return {
//       'id': id,
//       'name': name,
//       'status': status,
//     };
//   }
// }
