// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';
// import 'package:pin_input_text_field/pin_input_text_field.dart';
//
// class SmsReg extends StatefulWidget {
//   const SmsReg({Key? key}) : super(key: key);
//
//   @override
//   _SmsRegState createState() => _SmsRegState();
// }
//
// class _SmsRegState extends State<SmsReg> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         elevation: 0,
//         backgroundColor: Colors.white,
//         centerTitle: true,
//         title: Text(
//           'Подтверждение кода',
//           style: TextStyle(
//               color: Colors.black,
//               fontSize: 20.sp,
//               fontWeight: FontWeight.w600),
//         ),
//       ),
//       backgroundColor: Colors.white,
//       body: Padding(
//         padding: EdgeInsets.symmetric(horizontal: 28.w),
//         child: Column(
//           children: [
//             SizedBox(
//               width: 319.w,
//               child: Column(
//                 children: [
//                   Padding(
//                     padding: EdgeInsets.only(bottom: 56.w, top: 26.w),
//                     child: Row(
//                       children: [
//                         RichText(
//                           text: TextSpan(
//                             text: 'На номер ',
//                             style: TextStyle(
//                                 fontSize: 18.sp,
//                                 fontWeight: FontWeight.w500,
//                                 color: Colors.black),
//                             children: [
//                               TextSpan(
//                                 text: '#phone_number',
//                                 style: TextStyle(
//                                     fontWeight: FontWeight.w500,
//                                     fontSize: 18.sp,
//                                     color: mainColor),
//                               ),
//                               TextSpan(
//                                 text: '\nотправлен код подтверждения',
//                                 style: TextStyle(
//                                     fontSize: 18.sp,
//                                     fontWeight: FontWeight.w500,
//                                     color: Colors.black),
//                               ),
//                             ],
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   Padding(
//                     padding: EdgeInsets.symmetric(horizontal: 24.w),
//                     child: SizedBox(
//                       child: PinInputTextFormField(
//                         autoFocus: true,
//                         pinLength: 4,
//                         decoration: BoxLooseDecoration(
//                           bgColorBuilder: PinListenColorBuilder(
//                               Color(0xFFF5F6FA), Color(0xFFF5F6FA)),
//                           strokeColorBuilder: PinListenColorBuilder(
//                               Color(0xFFF5F6FA), Color(0xFFF5F6FA)),
//                         ),
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: EdgeInsets.only(top: 16.w),
//                     child: Row(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Padding(
//                           padding: EdgeInsets.only(top: 10.w),
//                           child: Text(
//                             'Не пришел код?',
//                             style: TextStyle(
//                                 color: Color(0xFF7B7890), fontSize: 14.sp),
//                           ),
//                         ),
//                         TextButton(
//                             style: TextButton.styleFrom(
//                                 padding: EdgeInsets.zero,
//                                 tapTargetSize:
//                                     MaterialTapTargetSize.shrinkWrap),
//                             onPressed: () {},
//                             child: Text(
//                               'Повторить',
//                               style:
//                                   TextStyle(color: mainColor, fontSize: 14.sp),
//                             ))
//                       ],
//                     ),
//                   )
//                 ],
//               ),
//             ),
//             Padding(
//               padding: EdgeInsets.only(top: 150.w),
//               child: TextButton(
//                   style: TextButton.styleFrom(
//                     backgroundColor: mainColor,
//                     padding:
//                         EdgeInsets.symmetric(horizontal: 119.w, vertical: 15.w),
//                   ),
//                   onPressed: () {},
//                   child: Text(
//                     'Отправить',
//                     style: TextStyle(color: Colors.white, fontSize: 16.sp),
//                   )),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
