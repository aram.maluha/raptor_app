// import 'package:equatable/equatable.dart';
// import 'package:meta/meta.dart';
// //
// class ObjectEntity extends Equatable {
//   final int? id;
//   final String? name;
//   final String? address;
//   final bool? status;
//   final List<StageEntity?>? stages;
//   final bool? techManagement;
//   final bool? alertManagement;
//   final bool? secureManagement;
//
//   ObjectEntity({
//     @required this.id,
//     @required this.name,
//     @required this.status,
//     @required this.address,
//     @required this.stages,
//     @required this.alertManagement,
//     @required this.secureManagement,
//     @required this.techManagement,
//   });
//
//   @override
//   List<Object?> get props => [
//         id,
//         name,
//         status,
//         address,
//         alertManagement,
//         secureManagement,
//         stages,
//         techManagement,
//       ];
// }
//
// class StageEntity {
//   final int? id;
//   final String? name;
//   final bool? status;
//
//   const StageEntity({@required this.id, @required this.name, @required this.status});
// }
