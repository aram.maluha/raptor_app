// import 'package:dartz/dartz.dart';
// import 'package:equatable/equatable.dart';
// import 'package:meta/meta.dart';
// import 'package:raptor_app/app/domain/entities/object_entity.dart';
// import 'package:raptor_app/app/domain/repositories/object_repository.dart';
// import 'package:raptor_app/core/error/failure.dart';
// import 'package:raptor_app/core/usecases/usecase.dart';
//
// class ObjectFetch extends UseCase<List<ObjectEntity>, PageObjectParams>{
//   final ObjectRepository characterRepository;
//
//   ObjectFetch(this.characterRepository);
//
//   Future<Either<Failure, List<ObjectEntity>>> call() async{
//     return characterRepository.getAllObjects();
//   }
// }
//
// class PageObjectParams extends Equatable{
//   final int? page;
//
//   PageObjectParams({@required this.page});
//
//   @override
//   List<Object?> get props => [page];
//
// }
