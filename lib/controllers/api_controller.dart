import 'dart:async';

import 'package:dio/dio.dart';
import 'package:get/get.dart' hide FormData;
import 'package:raptor_app/models/alerts_list_model.dart';
import 'package:raptor_app/models/events_list_model.dart';
import 'package:raptor_app/models/object_list_model.dart';
import 'package:raptor_app/models/org_models/org_contacts_model.dart';
import 'package:raptor_app/models/section_models/alert_button_switch_model.dart';
import 'package:raptor_app/models/section_models/controls_button_switch_model.dart';
import 'package:raptor_app/models/section_models/controls_list_model.dart';
import 'package:raptor_app/models/section_models/notification_list.dart';
import 'package:raptor_app/models/section_models/notification_switch.dart';
import 'package:raptor_app/models/section_models/section_switch_model.dart';
import 'package:raptor_app/models/security_button_switch.dart';
import 'package:raptor_app/models/user_location_model.dart';
import 'package:raptor_app/services/shared_preferences.dart';

import '../models/auth_models/first_auth_step.dart';
import '../models/auth_models/first_reg_step.dart';
import '../models/auth_models/second_reg_step.dart';
import '../models/org_models/org_list_model.dart';
import '../models/section_models/alert_button_model.dart';
import '../models/section_models/sections_list_model.dart';
import '../services/response_result.dart';
import '../services/rest_service.dart';
import '../services/settings.dart';

class ApiController extends GetxController {
  RestService restService = RestService();
  bool loading = false.obs();
  String prefix = ''.obs();
  String number = ''.obs();
  int? sectionId = 0.obs();
  int? controlId = 0.obs();
  int? buttonId = 0.obs();
  bool isOrgLength = false.obs();
  String? timeoutKey = ''.obs();
  int? organizationId = 0.obs();
  int? objectId = 0.obs();
  String? json;
  String token = ''.obs();
  String? deviceToken = ''.obs();
  UserLocationModel? userLocationModel = UserLocationModel().obs();
  FirstStepAuthAnswer? firstStepAuthAnswer = FirstStepAuthAnswer().obs();
  OrganizationsListAnswer? organizationsListAnswer =
      OrganizationsListAnswer().obs();

  ObjectList? objectList = ObjectList().obs();
  SectionsListAnswer? sectionsListAnswer = SectionsListAnswer().obs();
  ControlsListAnswer? controlsListAnswer = ControlsListAnswer().obs();
  NotificationListAnswer? notificationListAnswer =
      NotificationListAnswer().obs();
  OrgContactsAnswer? orgContactsAnswer = OrgContactsAnswer().obs();
  AlertButton? alertButton = AlertButton().obs();
  AlertButtonSwitch? alertButtonSwitch = AlertButtonSwitch().obs();
  EventListAnswer? eventListAnswer = EventListAnswer().obs();
  AlertListAnswer? alertListAnswer = AlertListAnswer().obs();
  NotificationSwitchAnswer? notificationSwitchAnswer =
      NotificationSwitchAnswer().obs();
  SectionSwitchSomeAnswer? sectionSwitchSomeAnswer =
      SectionSwitchSomeAnswer().obs();
  SecurityButtonSwitchAnswer? securityButtonSwitchAnswer =
      SecurityButtonSwitchAnswer().obs();
  int? sectionNumber = 0.obs();
  int? isPressed = 0.obs();
  int? status = 0.obs();

  //REG_FIRST
  Future<Result> firstRegStep(FirstStepReg model) async {
    loading = true;
    update();
    FormData formData = FormData.fromMap({
      'prefix': model.prefix,
      'phone_number': model.phoneNumber,
    });
    final firstStep = restService.request(
      REG_FIRST,
      method: POST,
      data: formData,
    );
    var result = await firstStep;
    print(result.data);
    if (result.status == Status.success) {
      prefix = model.prefix;
      number = model.phoneNumber;
      loading = false;
      update();
    } else if (result.status == Status.error) {
      loading = false;
      update();
    }
    return result;
  }

  //REG_SECOND
  Future<Result> secondRegStep(RegStepSecond model) async {
    loading = true;
    update();
    FormData formData = FormData.fromMap({
      'phone_number': number,
      'prefix': prefix,
      'verification_code': model.verificationCode,
      'password': model.password,
    });
    final secondStep = restService.request(
      REG_SECOND,
      method: POST,
      data: formData,
    );
    var result = await secondStep;
    if (result.status == Status.success) {
      loading = false;
      update();
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    } else if (result.status == Status.error || result.statusCode != 500) {
      loading = false;
      update();
    }
    print([result.data, result.status, result.statusCode]);
    return result;
  }

  //AUTH_FIRST
  Future<Result> firstStepAuth(FirstAuthStepRequest model) async {
    FormData formData = FormData.fromMap({
      'phone_number': model.phoneNumber,
      'prefix': model.prefix,
      'password': model.password,
      'device_token': deviceToken,
    });
    final firstLoginStep = restService.request(
      AUTH_FIRST,
      method: POST,
      data: formData,
    );
    var result = await firstLoginStep;
    if (result.status == Status.success) {
      status = result.statusCode;
      number = model.phoneNumber!;
      firstStepAuthAnswer = FirstStepAuthAnswer.fromJson(result.data);
      token = firstStepAuthAnswer!.data!.token!;
      PreferencesService.setNumber(model.phoneNumber!);
      PreferencesService.setPrefix(model.prefix!);
      PreferencesService.setToken(firstStepAuthAnswer!.data!.token!);
      print([
        PreferencesService.getOrgId(),
        PreferencesService.getNumber(),
        PreferencesService.getToken(),
      ]);
    }

    return result;
  }

  //ORG_LIST
  Future<Result> organizationsList() async {
    FormData formData = FormData.fromMap({
      'phone_number': number,
    });
    final orgList = restService.request(
      ORG_LIST,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await orgList;
    print(
        [result.errorText, result.status == Status.success, result.statusCode]);
    if (result.status == Status.success) {
      organizationsListAnswer = OrganizationsListAnswer.fromJson(result.data);
    }
    return result;
  }

  //ORG_CHOOSE
  Future<Result> organizationsChoose(int id) async {
    FormData formData = FormData.fromMap({
      'phone_number': number,
      'organization_id': id,
    });
    final orgChoose = restService.request(
      ORG_CHOOSE,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await orgChoose;
    if (result.status == Status.success) {
      organizationId = id;
      PreferencesService.setOrgId(id);
    }

    return result;
  }

  //LOG_OUT
  Future<Result> logOut(String orgId) async {
    FormData formData = FormData.fromMap({
      "phone_number": number,
      "organization_id": orgId,
    });
    final logOutAuth = restService.request(
      LOG_OUT,
      method: POST,
      data: formData,
    );
    var result = await logOutAuth;
    print([result.errorText, result.status, result.statusCode]);
    return result;
  }

  //OBJECT_LIST
  Future<Result> objectModel() async {
    loading = true;

    FormData formData = FormData.fromMap({
      "organization_id": organizationId,
      "phone_number": number,
    });
    final objectModelRequest = restService.request(
      OBJECT_LIST,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await objectModelRequest;
    if (result.status == Status.success) {
      objectList = ObjectList.fromJson(result.data);
      loading = false;
      update();
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    } else if (result.status == Status.error || result.statusCode != 500) {
      loading = false;
      update();
    }
    print([result.errorText, result.status, result.statusCode, result.data]);
    return result;
  }

  //ORGANIZATION CONTACTS
  Future<Result> orgContactsModel() async {
    loading = true;
    FormData formData = FormData.fromMap({
      "phone_number": number,
      "organization_id": organizationId,
    });
    final orgContactRequest = restService.request(
      CONTACTS_ORG,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await orgContactRequest;
    if (result.status == Status.success) {
      loading = false;
      update();
      orgContactsAnswer = OrgContactsAnswer.fromJson(result.data);
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    } else if (result.status == Status.error || result.statusCode != 500) {
      loading = false;
      update();
    }
    print([result.errorText, result.status, result.data]);
    return result;
  }

  //OBJECT NAME EDIT
  Future<Result> objectNameEdit(String objectLabel) async {
    FormData formData = FormData.fromMap({
      "organization_id": organizationId,
      "phone_number": number,
      "object_id": objectId,
      "object_label": objectLabel,
    });
    final objectNameEdit = restService.request(
      OBJECT_NAME_EDIT,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await objectNameEdit;
    print([result.errorText, result.status, result.statusCode]);
    return result;
  }

  //SECTIONS LIST
  Future<Result> sectionsModel() async {
    sectionsListAnswer = null;
    loading = true;

    FormData formData = FormData.fromMap({
      "object_id": objectId,
      "section_id": sectionId,
      "phone_number": number,
    });
    final sectionsListRequest = restService.request(
      OBJECT_SECTIONS,
      method: POST,
      data: formData,
      token: token,
    );
    var result = await sectionsListRequest;
    if (result.status == Status.success) {
      sectionsListAnswer = SectionsListAnswer.fromJson(result.data);
      loading = false;
      update();
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    } else if (result.status == Status.error || result.statusCode != 500) {
      loading = false;
      update();
    }

    print([result.status, result.errorText, result.statusCode, result.data]);
    return result;
  }

  // SECTIONS NAME EDIT
  Future<Result> sectionsNameEdit(String sectionLabel) async {
    FormData formData = FormData.fromMap({
      "object_id": objectId,
      "phone_number": number,
      "section_id": sectionId,
      "section_label": sectionLabel,
    });
    final sectionsNameEdit = restService.request(
      SECTIONS_NAME_EDIT,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await sectionsNameEdit;
    print([result.errorText, result.status, result.statusCode]);
    return result;
  }

  // CONTROLS NAME EDIT
  Future<Result> controlsNameEdit(String controlLabel) async {
    FormData formData = FormData.fromMap({
      "object_id": objectId,
      "phone_number": number,
      "control_id": controlId,
      "control_label": controlLabel,
    });
    final controlsNameEdit = restService.request(
      CONTROLS_NAME_EDIT,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await controlsNameEdit;
    print([result.errorText, result.status, result.statusCode]);
    return result;
  }

  Future<Result> getAlertButton() async {
    alertButton = null;
    loading = true;

    FormData formData = FormData.fromMap({
      "object_id": objectId,
      "phone_number": number,
    });
    final alertButtonModelRequest = restService.request(
      ALERT_BUTTON,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await alertButtonModelRequest;
    if (result.status == Status.success) {
      alertButton = AlertButton.fromJson(result.data);
      print('success');
      loading = false;
      update();
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    } else if (result.status == Status.error || result.statusCode != 500) {
      loading = false;
      update();
    }

    print([result.errorText, result.status, result.statusCode]);
    print([alertButton?.status, alertButton?.message]);
    return result;
  }

  Future<Result> alertButtonSwitchModel(
      AlertButtonSwitchRequest alertButtonSwitchRequest) async {
    FormData formData = FormData.fromMap({
      "button_id": buttonId,
      "timeout_key": timeoutKey,
    });
    final alertButtonSwitchRequest = restService.request(
      ALERT_BUTTON_SWITCH,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await alertButtonSwitchRequest;
    print([
      result.errorText,
      result.statusCode,
      result.status,
      // result.data,
    ]);
    return result;
  }

  Future<Result> controlsModel() async {
    controlsListAnswer = null;
    loading = true;

    FormData formData = FormData.fromMap({
      "object_id": objectId,
      "phone_number": number,
    });
    final controlsListModelRequest = restService.request(
      CONTROLS_LIST,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await controlsListModelRequest;
    if (result.status == Status.success) {
      controlsListAnswer = ControlsListAnswer.fromJson(result.data);
      loading = false;
      update();
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    } else if (result.status == Status.error || result.statusCode != 500) {
      loading = false;
      update();
    }

    print([result.errorText, result.status, result.statusCode, result.data]);
    return result;
  }

  // CONTROL SWITCH
  Future<Result> sectionSwitchModel(SectionSwitchRequest model) async {
    FormData formData = FormData.fromMap({
      "phone_number": number,
      "control_id": model.control_id,
      "control_state": model.control_state,
      "object_id": objectId,
    });
    final sectionSwitchRequest = restService.request(
      CONTROLS_SWITCH,
      method: POST,
      data: formData,
      token: token,
    );
    var result = await sectionSwitchRequest;
    print([result.data, result.statusCode, result.errorText]);
    print(model.control_state);
    // if (result.status == Status.success) {
    //   sectionsListAnswer = SectionsListAnswer.fromJson(result.data);
    // }
    return result;
  }

  //SECURITY BUTTON
  Future<Result> securityButtonModel(int state) async {
    loading = true;
    update();
    FormData formData = FormData.fromMap({
      "phone_number": number,
      "section_id": sectionId,
      "section_state": state,
    });
    final securityButtonSwitchRequest = restService.request(
      OBJECT_SECURITY_SWITCH,
      method: POST,
      data: formData,
      token: token,
    );
    var result = await securityButtonSwitchRequest;

    if (result.status == Status.success) {
      loading = false;
      update();
      sectionSwitchSomeAnswer = SectionSwitchSomeAnswer.fromJson(result.data);
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    } else if (result.status == Status.error || result.statusCode != 500) {
      loading = false;
      update();
    }
    return result;
  }

  //ALERT LIST
  Future<Result> alertListModel(String firstDate, String secondDate) async {
    FormData formData = FormData.fromMap({
      "phone_number": number,
      "object_id": objectId,
      "section_number": sectionNumber,
      "date_from": firstDate,
      "date_to": secondDate,
    });
    print([number, objectId, sectionNumber, firstDate, secondDate]);
    final alertListRequest = restService.request(
      OBJECT_THREATS,
      method: POST,
      data: formData,
      token: token,
    );
    var result = await alertListRequest;
    print([result.status, result.data, result.statusCode, result.errorText]);
    if (result.status == Status.success) {
      alertListAnswer = AlertListAnswer.fromJson(result.data);
    }

    return result;
  }

  // EVENTS LIST  //
  Future<Result> eventListModel(String firstDate, String secondDate) async {
    FormData formData = FormData.fromMap({
      "phone_number": number,
      "object_id": objectId,
      "section_number": sectionNumber,
      "date_from": firstDate,
      "date_to": secondDate,
    });

    print([
      firstDate,
      secondDate,
      number,
      objectId,
      sectionNumber,
    ]);
    final eventListRequest = restService.request(
      OBJECT_EVENTS,
      method: POST,
      data: formData,
      token: token,
    );
    var result = await eventListRequest;
    print([result.status, result.data, result.statusCode, result.errorText]);
    if (result.status == Status.success) {
      eventListAnswer = EventListAnswer.fromJson(result.data);
    }
    return result;
  }

  Future<Result> notificationListModel() async {
    notificationListAnswer = null;
    loading = true;
    FormData formData = FormData.fromMap({
      "section_id": sectionId,
      "phone_number": number,
    });
    final notificationListRequest = restService.request(
      OBJECT_NOTIFICATIONS,
      method: POST,
      token: token,
      data: formData,
    );
    var result = await notificationListRequest;
    if (result.status == Status.success) {
      notificationListAnswer = NotificationListAnswer.fromJson(result.data);
      loading = false;
      update();
    }
    print([result.errorText, result.status, result.statusCode, result.data]);
    return result;
  }

  Future<Result> notificationSwitchModel(
      NotificationSwitchRequest model) async {
    loading = true;
    update();
    FormData formData = FormData.fromMap({
      "phone_number": number,
      "section_id": sectionId,
      "threats": model.threats,
      "security": model.security,
      "tech": model.tech,
    });
    print(formData.fields);
    final notificationSwitchRequest = restService.request(
      OBJECT_NOTIFICATIONS_SWITCH,
      method: POST,
      data: formData,
      token: token,
    );
    var result = await notificationSwitchRequest;
    if (result.status == Status.success) {
      await notificationListModel();
      loading = false;
      update();
    } else if (result.statusCode == 500) {
      loading = false;
      update();
    }

    print([result.data, result.statusCode, result.errorText]);
    return result;
  }

  Future<Result> userLocation() async {
    final userLocation = restService.request(
      USER_LOCATION,
      method: GET,
    );
    var result = await userLocation;
    if (result.status == Status.success) {
      UserLocationModel.fromJson(result.data);
    }
    print([result.errorText, result.status, result.statusCode, result.data]);
    return result;
  }
}

///____________________________________________\\\
///--------------------------------------------\\\
///        ___                                 \\\
///       ///\\     ||-----||   \\    //       \\\
///      ///  \\    ||     ||    \\__//        \\\
///     ///    \\   ||     ||    //--\\        \\\
///    ///      \\  ||_____||   //    \\       \\\
///____________________________________________\\\
///--------------------------------------------\\\
