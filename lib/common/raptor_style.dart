import 'package:flutter/material.dart';
import 'package:raptor_app/app/presentation/widgets/main_widgets.dart';

class RaptorStyle {
  static BoxDecoration kTopTextDecoration = BoxDecoration(
    border: Border(
      bottom: BorderSide(width: 5.0, color: mainColor),
    ),
  );
  static BoxDecoration kTopTextUnselectedDecoration = BoxDecoration(
    border: Border(),
  );
}
