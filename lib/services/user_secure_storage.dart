import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class UserSecureStorage {
  static final _storage = FlutterSecureStorage();

  static const _keyUserToken = 'userToken';
  static const _keyUserNumber = 'userNumber';
  static const _keyUserOrgId = 'userOrgId';
  static const _keyUserPass = 'userPass';
  static const _keyPrefix = 'prefix';

  static Future setUserToken(String userToken) async =>
      await _storage.write(key: _keyUserToken, value: userToken);

  static Future<String?> getUserToken() async =>
      await _storage.read(key: _keyUserToken);

  static Future setUserNumber(String userNumber) async =>
      await _storage.write(key: _keyUserNumber, value: userNumber);

  static Future<String?> getUserNumber() async =>
      await _storage.read(key: _keyUserNumber);

  static Future setOrgId(String userOrgId) async =>
      await _storage.write(key: _keyUserOrgId, value: userOrgId);

  static Future setPrefix(String prefix) async =>
      await _storage.write(key: _keyPrefix, value: prefix);

  static Future<String?> getPrefix() async =>
      await _storage.read(key: _keyPrefix);

  static Future<String?> getUserOrgId() async =>
      await _storage.read(key: _keyUserOrgId);

  static Future setUserPass(String userPass) async =>
      await _storage.write(key: _keyUserPass, value: userPass);

  static Future<String?> getUserPass() async =>
      await _storage.read(key: _keyUserPass);
}
