const BASE_URL = "http://raptor.sport-market.kz";
const BASE_API = "$BASE_URL/api/";

const REG_FIRST = "v1/auth/register/step-one";
const REG_SECOND = "v1/auth/register/step-two";
const AUTH_FIRST = "v1/auth/login";
const ORG_LIST = "v1/organizations";
const ORG_CHOOSE = "v1/organization";
const LOG_OUT = "v1/auth/logout";
const CONTACTS_ORG = "v1/organization/contacts";
const OBJECT_LIST = "v1/organization/object";
const ALERT_BUTTON = "v1/organization/object/button";
const ALERT_BUTTON_SWITCH = "v1/organization/object/button/switch";
const CONTROLS_LIST = "v1/organization/object/controls";
const CONTROLS_SWITCH = "v1/organization/object/controls/switch";
const OBJECT_SECTIONS = "v1/organization/object/sections";
const OBJECT_SECURITY_SWITCH = "v1/organization/object/sections/switch";
const OBJECT_THREATS = "v1/organization/object/threats";
const OBJECT_EVENTS = "v1/organization/object/events";
const OBJECT_NOTIFICATIONS = "v1/organization/object/sections/notifications";
const OBJECT_NOTIFICATIONS_SWITCH =
    "v1/organization/object/sections/notifications/switch";
const OBJECT_NAME_EDIT = "v1/organization/object/label";
const SECTIONS_NAME_EDIT = "v1/organization/object/sections/label";
const CONTROLS_NAME_EDIT = "v1/organization/object/controls/label";
const USER_LOCATION = "v1/location";