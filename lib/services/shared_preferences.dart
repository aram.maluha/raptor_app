import 'package:shared_preferences/shared_preferences.dart';

class PreferencesService {
  static SharedPreferences? _sharedPreferences;

  static const _token = 'token';
  static const _prefix = 'prefix';
  static const _phoneNumber = 'number';
  static const _orgId = 'orgId';

  static Future init() async =>
      _sharedPreferences = await SharedPreferences.getInstance();

  static Future setToken(String data) async =>
      _sharedPreferences?.setString(_token, data);
  static String getToken() => _sharedPreferences?.getString(_token) ?? '';

  static Future setPrefix(String data) async =>
      _sharedPreferences?.setString(_prefix, data);
  static String getPrefix() => _sharedPreferences?.getString(_prefix) ?? '';

  static Future setNumber(String data) async =>
      _sharedPreferences?.setString(_phoneNumber, data);
  static String getNumber() =>
      _sharedPreferences?.getString(_phoneNumber) ?? '';

  static Future setOrgId(int data) async =>
      _sharedPreferences?.setInt(_orgId, data);
  static int getOrgId() => _sharedPreferences?.getInt(_orgId) ?? 0;
}
