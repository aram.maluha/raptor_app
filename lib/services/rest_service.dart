import 'dart:io';

import 'package:dio/dio.dart';

import 'response_result.dart';
import 'settings.dart';

const GET = 'get';
const POST = 'post';
const PUT = 'put';
const DELETE = 'delete';

class RestService {
  Dio? _dio;

  RestService() {
    _dio = Dio(_options);
  }

  BaseOptions _options = new BaseOptions(
    baseUrl: BASE_API,
    connectTimeout: 60000,
    receiveTimeout: 60000,
    sendTimeout: 60000,
    headers: {'Accept': 'application/json'},
  );

  Future<Result<dynamic>> request(String url,
      {String method = GET,
      data,
      Map<String, dynamic>? queryParameters,
      String token = ''}) async {
    try {
      if (token != '') _options.headers["Authorization"] = '$token';
      switch (method) {
        case POST:
          Response response = await _dio!.post(url, data: data);
          return Result<dynamic>(data: response.data, status: Status.success);
        case PUT:
          Response response = await _dio!.put(url, data: data);
          return Result<dynamic>(data: response.data, status: Status.success);
        case DELETE:
          Response response =
              await _dio!.delete(url, queryParameters: queryParameters);
          return Result<dynamic>(data: response.data, status: Status.success);
        default:
          Response response =
              await _dio!.get(url, queryParameters: queryParameters);
          return Result<dynamic>(data: response.data, status: Status.success);
      }
      // ignore: unused_catch_stack
    } catch (e) {
      return Result(
          errorText: handleError(e), status: Status.error, statusCode: 500);
    }
  }
}
