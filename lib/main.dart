import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:raptor_app/app/presentation/pages/alerts_history.dart';
import 'package:raptor_app/app/presentation/pages/auth_screen.dart';
import 'package:raptor_app/app/presentation/pages/contacts_screen.dart';
import 'package:raptor_app/app/presentation/pages/enter_page.dart';
import 'package:raptor_app/app/presentation/pages/events_history.dart';
import 'package:raptor_app/app/presentation/pages/notifications_screen.dart';
import 'package:raptor_app/app/presentation/pages/object_sections.dart';
import 'package:raptor_app/app/presentation/pages/objects_screen.dart';
import 'package:raptor_app/app/presentation/pages/security_screen.dart';
import 'package:raptor_app/app/presentation/widgets/auth_forms/auth_second_form.dart';
import 'package:raptor_app/app/presentation/widgets/navbar_components.dart';
import 'package:raptor_app/controllers/api_controller.dart';
import 'package:raptor_app/services/shared_preferences.dart';

import 'app/presentation/pages/test_screen.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  NotificationDetails(
    android: AndroidNotificationDetails(
      channel.id,
      channel.name,
      channel.description,
    ),
    // )
  );
}

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
  enableVibration: true,
  playSound: true,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  WidgetsFlutterBinding.ensureInitialized();
  await PreferencesService.init();
  WidgetsFlutterBinding.ensureInitialized();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  _MyAppState({this.fcmDeviceToken});
  ApiController apiController = Get.put(ApiController());
  // final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  String? fcmDeviceToken;

  @override
  void initState() {
    super.initState();
    var initializationSettingsAndroid =
        AndroidInitializationSettings("@mipmap/ic_launcher");
    var initializationSettings =
        InitializationSettings(android: initializationSettingsAndroid);
    flutterLocalNotificationsPlugin.initialize(initializationSettings);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;
      if (notification != null && android != null) {
        // flutterLocalNotificationsPlugin.show(
        //     notification.hashCode,
        //     notification.title,
        //     notification.body,
        NotificationDetails(
          android: AndroidNotificationDetails(
            channel.id,
            channel.name,
            channel.description,
            icon: 'launch_background',
          ),
          // )
        );
      }
    });
    getToken();
    print("fcm token:$fcmDeviceToken");
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return Center(child: CircularProgressIndicator());
        }
        if (snapshot.connectionState == ConnectionState.done) {
          return ScreenUtilInit(
            designSize: Size(375, 812),
            builder: () => GetMaterialApp(
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                fontFamily: 'SF-PRO-ROUNDED',
              ),
              home: EntryApp(),
              getPages: [
                GetPage(
                  name: "/auth_screen",
                  page: () => AuthScreen(),
                ),
                GetPage(
                  name: "/contacts_screen",
                  page: () => ContactsScreen(),
                ),
                GetPage(
                  name: "/objects_screen",
                  page: () => ObjectsScreen(),
                ),
                GetPage(
                  name: "/alerts_history",
                  page: () => AlertsHistory(),
                ),
                GetPage(
                  name: "/events_history",
                  page: () => EventsHistory(),
                ),
                GetPage(
                  name: "/notifications_screen",
                  page: () => NotificationsScreen(),
                ),
                GetPage(
                  name: "/objects_sections",
                  page: () => ObjectSections(),
                ),
                GetPage(
                  name: "/security_screen",
                  page: () => SecurityScreen(),
                ),
              ],
            ),
          );
        }
        return Container();
      },
    );
  }

  void getToken() async {
    fcmDeviceToken = await FirebaseMessaging.instance.getToken();
    print('fcmDeviceToken: $fcmDeviceToken');
    apiController.deviceToken = fcmDeviceToken;
    // ApiController apiController = Get.put(ApiController());
    // fcmDeviceToken = await FirebaseMessaging.instance.getAPNSToken();
  }
}

Widget? getNavbar({bool? isContactScreen = false}) {
  if (!isContactScreen!) {
    return NavBarSecond();
  }
  return NavBarFirst();
}
